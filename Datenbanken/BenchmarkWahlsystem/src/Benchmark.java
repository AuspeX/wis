import java.util.ArrayList;
import java.util.List;

/**
 * This class Represents a Benchmarkrun
 * 
 * @author kaelumania
 *
 */
public class Benchmark {

	private static final String root = "http://localhost:8888/wahlsystem";
	private static final int DECKSIZE = 10;
	
	List<Terminal> clients;
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		//Different URLs to call service
		String getSitzverteilungQ1 = "7|0|4|http://127.0.0.1:8888/wahlsystem/|4E8AA8C12B50D9A25BB8304C84C11483|de.tum.wahlsystem.client.WahlinfoService|getSitzverteilung|1|2|3|4|0|";
		String getSitzbelegungQ2 = "7|0|4|http://127.0.0.1:8888/wahlsystem/|4E8AA8C12B50D9A25BB8304C84C11483|de.tum.wahlsystem.client.WahlinfoService|getSitzbelegung|1|2|3|4|0|";
		String getWahlkreisInfoQ3 = "7|0|5|http://127.0.0.1:8888/wahlsystem/|4E8AA8C12B50D9A25BB8304C84C11483|de.tum.wahlsystem.client.WahlinfoService|getWahlkreisInfo|I|1|2|3|4|1|5|25|";
		String getWahlkreisSiegerQ4 = "7|0|4|http://127.0.0.1:8888/wahlsystem/|4E8AA8C12B50D9A25BB8304C84C11483|de.tum.wahlsystem.client.WahlinfoService|getWahlkreisSieger|1|2|3|4|0|";
		String getUeberhangQ5 = "7|0|4|http://127.0.0.1:8888/wahlsystem/|4E8AA8C12B50D9A25BB8304C84C11483|de.tum.wahlsystem.client.WahlinfoService|getUeberhaenge|1|2|3|4|0|";
		String getKnappsteQ6 = "7|0|4|http://127.0.0.1:8888/wahlsystem/|4E8AA8C12B50D9A25BB8304C84C11483|de.tum.wahlsystem.client.WahlinfoService|getKnappste|1|2|3|4|0|";
		
		List<Workload> workloads = new ArrayList<Workload>();
		
		//Workloads with rate
		workloads.add(new Workload("getSitzverteilung",root, getSitzverteilungQ1, 0.25));
		workloads.add(new Workload("getSitzbelegung",root, getSitzbelegungQ2, 0.10));
		workloads.add(new Workload("getWahlkreisInfo",root, getWahlkreisInfoQ3, 0.25));
		workloads.add(new Workload("getWahlkreisSieger",root, getWahlkreisSiegerQ4, 0.10));
		workloads.add(new Workload("getUeberhang",root, getUeberhangQ5, 0.10));
		workloads.add(new Workload("getKnappste",root, getKnappsteQ6, 0.10));
		
		//Definied Parameter
		int t = 1000;//ms
		int clients = 10;
		
		//Create Benchmark
		Benchmark b = new Benchmark(workloads, t, clients);
		
		//Run Benchmark
		b.startBenchmark();
	}
	
	
	/**
	 * Create a custom Benchmark
	 * 
	 * @param workload - work to do
	 * @param avrIdletime - average idletime of a client/terminal
	 * @param numberClients - number of clients (threads)
	 */
	public Benchmark(List<Workload> workload, int avrIdletime, int numberClients)
	{
		//Print Configuration
		System.out.println("[avrIdletime = "+avrIdletime+", numberClients = "+numberClients+", decksize = "+DECKSIZE+"]");
		clients = new ArrayList<Terminal>();
		
		//Create clients
		while(clients.size() < numberClients)
			clients.add(new Terminal(workload, avrIdletime, DECKSIZE));
	}
	
	
	/**
	 * Starts all terminal/clients to work and waits for their results
	 */
	public void startBenchmark()
	{
		//Start Clients
		for(Terminal bc : clients)
			bc.start();
		
		//Wait for Clients to terminate
		for(Terminal bc : clients)
			try {
				bc.join();
			} catch (InterruptedException e) {}
		
		//Evaluate Results
		for(Terminal bc : clients)
			System.out.println("Result: "+ bc.getName() + " - " + bc.getClientResult());
	}

}
