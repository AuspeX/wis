import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.apache.commons.lang.time.StopWatch;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;


/**
 * Terminal class simulates a client, which randomly requests websites
 * 
 * @author kaelumania
 *
 */
public class Terminal extends Thread
{
	List<Workload> workload; 
	int averageIdletime;
	int numberCalls;
	
	double clientResult = 0.0;
	
	StopWatch watch;
	
	/**
	 * Creates a new Terminal, which fires url requests
	 * 
	 * @param workload - available urls
	 * @param avrIdletime - average idletime between calls
	 * @param numberCalls - total number of calls to do
	 */
	public Terminal(List<Workload> workload, int avrIdletime, int numberCalls)
	{
		this.workload = workload;
		this.averageIdletime = avrIdletime;
		this.numberCalls = numberCalls;
		
		//Init Watch
		watch = new StopWatch();
	}
	
	public void run()
	{
		double result = 0.0;
		int works = 0;
		
		try{
			//Manage Workloads
			Hashtable<Workload, Integer> carddeck = new Hashtable<Workload, Integer>();
			for(int i = 0; i < workload.size(); i++)
				carddeck.put(workload.get(i), new Integer((int) Math.round(numberCalls * workload.get(i).getRate())));
			
			//Do all workloads
			List<Workload> currentWorkload = new ArrayList<Workload>(workload);
			while(!currentWorkload.isEmpty())
			{
				//idle
				long idletime = Math.round((Math.random()*(averageIdletime*0.4))+(averageIdletime*0.8));	
			
				try {
					//System.out.println("sleep "+idletime);
					Thread.sleep(idletime);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				//Select random workload
				int randomIndex = (int) Math.round(Math.random()*(currentWorkload.size()-1));
				
				//Select next work
				Workload w = currentWorkload.get(randomIndex);
				
				//Do work
				result += this.work(w);
				works++;
				
				//Remove
				int cards = carddeck.get(w);
				cards--;//Take card
				carddeck.put(w, cards);
				
				//If workload has no more cards, remove...
				if(cards <= 0)
					currentWorkload.remove(w);
				
				
			}
			
			//Set Result
			clientResult = result/(works != 0 ? works : 1);
		}catch(Exception e){
			System.out.println(this.getName() + " crashed "+ e.getMessage());
		}
		
	}

	
	/**
	 * Executes an URL call
	 * 
	 * @param work - URL to execute
	 * @return time used to execute call
	 * 
	 * @throws Exception
	 */
	private double work(Workload work) throws Exception
	{
		//Setup POST
		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost("http://localhost:8888/wahlsystem/greet");
		
			StringEntity entity = new StringEntity(work.getUrl(),"text/x-gwt-rpc", "UTF-8");
			
			post.setEntity(entity);
			post.addHeader("Connection", "keep-alive");
			post.addHeader("X-GWT-Module-Base", "i dont care");//Egal
			post.addHeader("X-GWT-Permutation", "5FBF73B0E2D3AE13460523221A778A4B");//Egal
			
			HttpResponse response = null;
			HttpEntity rEntity = null;
			
			//Execute POST Call
			
			//synchronized (this) {
				//System.out.println(this.getName()+ " do "+ work.getName());
				watch.reset();
				watch.start();
				response = client.execute(post);//this method needs time
			
				rEntity = response.getEntity();
				watch.stop();
			//}
				//Check time
				if(rEntity != null){
	                System.out.println(this.getName()+ " "+ work.getName() +" "+watch.getTime()+ " ms");
	                return watch.getTime();
				
			}
			
		return 0;
	}
	
	/**
	 * Returns the Result, if the thread has finished
	 * @return
	 */
	public double getClientResult() {
		return clientResult;
	}
	
}
