/**
 * Workload class represents a specific task (calling URL) multiple times
 * 
 * @author kaelumania
 *
 */
public class Workload {

	String name;
	String baseUrl;
	String url;
	double rate;
	
	/**
	 * Create a workload
	 * 
	 * @param name - name of the work
	 * @param baseUrl - baseURL of the server
	 * @param url - URL to call
	 * @param rate - Rate to call the url
	 */
	public Workload(String name, String baseUrl, String url, double rate) {
		super();
		
		this.name = name;
		this.url = url;
		this.rate = rate;
		this.baseUrl = baseUrl;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public double getRate() {
		return rate;
	}

	public void setRate(double haeufigkeit) {
		this.rate = haeufigkeit;
	}

	public String getBaseUrl() {
		return baseUrl;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
