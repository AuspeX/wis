﻿insert into "Politiker" (politiker_vorname,politiker_nachname,politiker_jahrgang,politiker_partei)
(
	select vorname,nachname,jahrgang,partei from "csv_Wahlbewerber_2009"
	union
	select vorname,nachname,jahrgang,partei from "csv_Wahlbewerber_2005" w where not exists
	(
		select * from "csv_Wahlbewerber_2005" as o 
		inner join "csv_Wahlbewerber_2009" as n ON n.nachname = o.nachname and n.vorname = o.vorname and n.jahrgang = o.jahrgang
		where n.nachname = w.nachname and n.vorname = w.vorname and n.jahrgang = w.jahrgang
	)
)