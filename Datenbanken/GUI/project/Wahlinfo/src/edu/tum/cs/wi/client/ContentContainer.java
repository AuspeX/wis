package edu.tum.cs.wi.client;

import com.google.gwt.user.client.ui.RootPanel;

public class ContentContainer {
	
	private static ContentContainer container = null;
	private static RootPanel contentPanel = null;
	
	private ContentContainer() {}
	
	public static ContentContainer getInstance() {
		if (container == null) {
			container = new ContentContainer();
		}
		return container;
	}

	public void setContentPanel(RootPanel rootPanel) {
		contentPanel = rootPanel;
	}
	
	public void clearContent() {
		contentPanel.clear();
//		contentPanel.setVisible(false);
	}

}