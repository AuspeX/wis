﻿CREATE OR REPLACE VIEW "Result_Direktkandidaten_2009_Stimmzettel" AS 
 SELECT kandidat.stimme_wahlkreis, kandidat.stimme_anzahl, kandidat.erststimme_direktkandidat
   FROM "Erststimmen_Stimmzettel" kandidat
   JOIN ( SELECT max(win.stimme_anzahl) AS stimme_anzahl, win.stimme_wahlkreis
           FROM "Erststimmen_Stimmzettel" win
          WHERE win.wahlzettel_gueltig AND win.stimme_wahl = 2009
          GROUP BY win.stimme_wahlkreis) winner ON winner.stimme_wahlkreis = kandidat.stimme_wahlkreis
  WHERE kandidat.wahlzettel_gueltig AND kandidat.stimme_wahl = 2009 AND kandidat.stimme_anzahl >= winner.stimme_anzahl
  ORDER BY kandidat.stimme_wahlkreis