﻿CREATE OR REPLACE VIEW "Result_Parteien_Wahlkreis_Stimmzettel" AS 
 SELECT COALESCE(estimmen.wahlkreis, zstimmen.wahlkreis) AS wahlkreis, COALESCE(estimmen.partei, zstimmen.partei) AS partei, COALESCE(estimmen.wahl, zstimmen.wahl) AS wahl, estimmen.erststimmen, zstimmen.zweitstimmen
   FROM ( SELECT "Erststimmen_Stimmzettel".stimme_wahlkreis AS wahlkreis, "Erststimmen_Stimmzettel".stimme_wahl AS wahl, sum("Erststimmen_Stimmzettel".stimme_anzahl) AS erststimmen, "Mitglieder".mitglied_partei AS partei
           FROM "Erststimmen_Stimmzettel", "Direktkandidaten", "Politiker", "Mitglieder"
          WHERE "Erststimmen_Stimmzettel".erststimme_direktkandidat IS NOT NULL AND "Direktkandidaten".direktkandidat_id = "Erststimmen_Stimmzettel".erststimme_direktkandidat AND "Direktkandidaten".direktkandidat_politiker = "Politiker".politiker_id AND "Politiker".politiker_id = "Mitglieder".mitglied_politiker AND "Mitglieder".mitglied_jahr = "Erststimmen_Stimmzettel".stimme_wahl
          GROUP BY "Erststimmen_Stimmzettel".stimme_wahl, "Erststimmen_Stimmzettel".stimme_wahlkreis, "Mitglieder".mitglied_partei
          ORDER BY sum("Erststimmen_Stimmzettel".stimme_anzahl) DESC) estimmen
   LEFT JOIN ( SELECT "Zweitstimmen_Stimmzettel".stimme_wahlkreis AS wahlkreis, "Zweitstimmen_Stimmzettel".stimme_wahl AS wahl, sum("Zweitstimmen_Stimmzettel".stimme_anzahl) AS zweitstimmen, "Landeslisten".landesliste_partei AS partei
           FROM "Zweitstimmen_Stimmzettel", "Landeslisten"
          WHERE "Zweitstimmen_Stimmzettel".zweitstimme_landesliste IS NOT NULL AND "Landeslisten".landesliste_id = "Zweitstimmen_Stimmzettel".zweitstimme_landesliste
          GROUP BY "Zweitstimmen_Stimmzettel".stimme_wahl, "Zweitstimmen_Stimmzettel".stimme_wahlkreis, "Landeslisten".landesliste_partei
          ORDER BY sum("Zweitstimmen_Stimmzettel".stimme_anzahl) DESC) zstimmen ON estimmen.partei = zstimmen.partei AND estimmen.wahlkreis = zstimmen.wahlkreis AND estimmen.wahl = zstimmen.wahl
  ORDER BY COALESCE(estimmen.wahlkreis, zstimmen.wahlkreis), COALESCE(estimmen.wahl, zstimmen.wahl);