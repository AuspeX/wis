﻿CREATE VIEW "Zweitstimmen_Stimmzettel" AS (SELECT count(*) AS stimme_anzahl, (CASE WHEN (wahlzettel_zweitstimme != -1) THEN TRUE ELSE FALSE END) AS wahlzettel_gueltig, wahlzettel_wahlkreis AS stimme_wahlkreis, wahlzettel_wahl AS stimme_wahl, wahlzettel_zweitstimme AS zweitstimme_landesliste
FROM "Wahlzettel"
GROUP BY wahlzettel_wahlkreis, wahlzettel_wahl, wahlzettel_zweitstimme)