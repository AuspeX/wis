package de.tum.wahlsystem.client;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.visualization.client.VisualizationUtils;
import com.google.gwt.visualization.client.visualizations.GeoMap;
import com.google.gwt.visualization.client.visualizations.Table;
import com.google.gwt.visualization.client.visualizations.corechart.PieChart;

import de.tum.wahlsystem.client.widgets.AbstractWidget;
import de.tum.wahlsystem.client.widgets.GeoParteiWidget;
import de.tum.wahlsystem.client.widgets.GeoSiegerWidget;
import de.tum.wahlsystem.client.widgets.KnappsteSiegerWidget;
import de.tum.wahlsystem.client.widgets.MitgliederBundestagWidget;
import de.tum.wahlsystem.client.widgets.SitzverteilungWidget;
import de.tum.wahlsystem.client.widgets.UeberhangmandateWidget;
import de.tum.wahlsystem.client.widgets.WahlkreisWidget;
import de.tum.wahlsystem.client.widgets.WahlkreisWidgetStimmzettel;
import de.tum.wahlsystem.client.widgets.WahlkreissiegerWidget;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class WahlSystem implements EntryPoint {

	/**
	 * Create a remote service proxy to talk to the server-side Greeting service.
	 */
	private WahlinfoServiceAsync infoService;// = GWT.create(WahlinfoService.class);

	List<AbstractWidget> widgets = new ArrayList<AbstractWidget>();
	
	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		
		infoService = GWT.create(WahlinfoService.class);
		
		VisualizationUtils.loadVisualizationApi(new Runnable() 
		{
		      public void run() 
		      {
		    	  
		    	final TabPanel tabPanel = new TabPanel();
		    	tabPanel.addSelectionHandler(new SelectionHandler<Integer>() {
					
					@Override
					public void onSelection(SelectionEvent<Integer> event) {
						System.out.println("Selected: "+event.getSelectedItem());
						
						int index = event.getSelectedItem();
						
						if(widgets.size() > 0 && index >= 0 && index < widgets.size())
						{
							AbstractWidget w = widgets.get(index);
							w.load();
						}
					}
				});
		    	
		    	widgets.add(new SitzverteilungWidget(infoService));
		    	widgets.add(new MitgliederBundestagWidget(infoService));
		    	widgets.add(new WahlkreisWidget(infoService));
		    	widgets.add(new WahlkreissiegerWidget(infoService));
		    	widgets.add(new UeberhangmandateWidget(infoService));
		    	widgets.add(new KnappsteSiegerWidget(infoService));
		    	widgets.add(new WahlkreisWidgetStimmzettel(infoService));
		    	
		    	widgets.add(new GeoSiegerWidget(infoService));
		    	widgets.add(new GeoParteiWidget(infoService));
		    	
		        RootPanel.get().add(tabPanel);
				for(AbstractWidget w : widgets)
					tabPanel.add(w, w.getTitle());
					
				tabPanel.selectTab(0);
		      }}
		, PieChart.PACKAGE, Table.PACKAGE, GeoMap.PACKAGE);

	}
}
