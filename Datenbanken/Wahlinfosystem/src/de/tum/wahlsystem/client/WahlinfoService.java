package de.tum.wahlsystem.client;

import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import de.tum.wahlsystem.shared.BundeslandSieger;
import de.tum.wahlsystem.shared.BundeslandSiegerPartei;
import de.tum.wahlsystem.shared.Politiker;
import de.tum.wahlsystem.shared.PolitikerErgebnis;
import de.tum.wahlsystem.shared.Sitz;
import de.tum.wahlsystem.shared.Ueberhang;
import de.tum.wahlsystem.shared.WahlkreisInfo;
import de.tum.wahlsystem.shared.WahlkreisSieger;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("greet")
public interface WahlinfoService extends RemoteService 
{
	List<Sitz> getSitzverteilung();
	List<Politiker> getSitzbelegung();
	
	List<Ueberhang> getUeberhaenge();
	
	List<PolitikerErgebnis> getKnappste();
	
	WahlkreisInfo getWahlkreisInfo(int wahlkreisId);
	
	WahlkreisInfo getWahlkreisInfoStimmzettel(int wahlkreisId);
	
	List<WahlkreisSieger> getWahlkreisSieger();
	
	List<BundeslandSieger> getBundeslandSieger();
	
	List<BundeslandSiegerPartei> getBundeslandSiegerPartei(String partei);
}
