package de.tum.wahlsystem.client;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;

import de.tum.wahlsystem.shared.BundeslandSieger;
import de.tum.wahlsystem.shared.BundeslandSiegerPartei;
import de.tum.wahlsystem.shared.Politiker;
import de.tum.wahlsystem.shared.PolitikerErgebnis;
import de.tum.wahlsystem.shared.Sitz;
import de.tum.wahlsystem.shared.Ueberhang;
import de.tum.wahlsystem.shared.WahlkreisInfo;
import de.tum.wahlsystem.shared.WahlkreisSieger;

/**
 * The async counterpart of <code>GreetingService</code>.
 */
public interface WahlinfoServiceAsync 
{	
	void getSitzverteilung(AsyncCallback<List<Sitz>> callback);

	void getSitzbelegung(AsyncCallback<List<Politiker>> callback);

	void getUeberhaenge(AsyncCallback<List<Ueberhang>> callback);
	
	void getKnappste(AsyncCallback<List<PolitikerErgebnis>> callback);

	void getWahlkreisInfo(int wahlkreisId, AsyncCallback<WahlkreisInfo> callback);

	void getWahlkreisSieger(AsyncCallback<List<WahlkreisSieger>> callback);
	
	void getWahlkreisInfoStimmzettel(int walkreisId, AsyncCallback<WahlkreisInfo> callback);

	void getBundeslandSieger(AsyncCallback<List<BundeslandSieger>> callback);
	
	void getBundeslandSiegerPartei(String partei, AsyncCallback<List<BundeslandSiegerPartei>> callback);
}
