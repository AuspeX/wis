package de.tum.wahlsystem.client.widgets;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;

import de.tum.wahlsystem.client.WahlinfoServiceAsync;

public abstract class AbstractWidget extends FlowPanel {

	protected WahlinfoServiceAsync service;
	
	private Grid root;
	private Label infoLabel;
	
	private FlowPanel content;
	
	private String longTitle;
	
	public AbstractWidget(WahlinfoServiceAsync service, String title, String longTitle)
	{
		super();
		this.longTitle = longTitle;
		this.setTitle(title);
		this.service = service;
		
		infoLabel = new Label("Lade Daten...");
		infoLabel.setVisible(false);
		
		root = new Grid(3,1);
		root.setWidth("100%");
		
		root.setWidget(0, 0, new HTML("<h2>"+this.longTitle+"</h2>"));
		root.getCellFormatter().getElement(0, 0).setAttribute("align", "center");
		
		root.setWidget(1, 0, infoLabel);
		root.getCellFormatter().getElement(0, 0).setAttribute("align", "center");
		
		content = new FlowPanel();
		content.getElement().setAttribute("align", "center");
		
		root.setWidget(2, 0, content);
		root.getCellFormatter().getElement(1, 0).setAttribute("align", "center");
		
		this.add(root);
		
		this.initialize();
	}
	
	public abstract void initialize();
	
	public abstract void load();
	
	protected Panel getRoot()
	{
		return content;
	}
	
	public void setLoading(boolean loading)
	{
		if(loading)
			infoLabel.setVisible(true);
		else{
			infoLabel.setVisible(false);
		}
	}
}
