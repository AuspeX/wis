package de.tum.wahlsystem.client.widgets;

import java.util.List;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.visualization.client.AbstractDataTable.ColumnType;
import com.google.gwt.visualization.client.DataTable;
import com.google.gwt.visualization.client.visualizations.GeoMap;
import com.google.gwt.visualization.client.visualizations.GeoMap.DataMode;
import com.google.gwt.visualization.client.visualizations.GeoMap.Options;

import de.tum.wahlsystem.client.WahlinfoServiceAsync;
import de.tum.wahlsystem.shared.BundeslandSiegerPartei;

public class GeoParteiWidget extends AbstractWidget{

	DataTable data;
	
	GeoMap chart;
	Options options;
	
	ListBox parteiBox;
	
	private final static int CDU_COLOR = 0x000000;
	private final static int SPD_COLOR = 0xFF0000;
	private final static int LINKE_COLOR = 0x9932CC;
	
	private final static int BLANK_COLOR = 0xFFFFFF;
	
	public GeoParteiWidget(WahlinfoServiceAsync service) {
		super(service,"Geographisch II" ,"Geographische Ansicht Erststimmen");
	}

	@Override
	public void initialize() 
	{
		parteiBox = new ListBox(false);
		parteiBox.addItem("CDU");
		parteiBox.addItem("CSU");
		parteiBox.addItem("SPD");
		parteiBox.addItem("Die Linke");
		
		parteiBox.addChangeHandler(new ChangeHandler() {
			
			@Override
			public void onChange(ChangeEvent event) {
				load();
			}
		});
		
		getRoot().add(parteiBox);
	}
	
	public int getColor(String partei)
	{	
		if(partei.equalsIgnoreCase("cdu"))
			return CDU_COLOR;
		if(partei.equalsIgnoreCase("csu"))
			return CDU_COLOR;
		
		if(partei.equalsIgnoreCase("spd"))
			return SPD_COLOR;
		
		if(partei.equalsIgnoreCase("die linke"))
			return LINKE_COLOR;
		
		return 3;
	}

	@Override
	public void load() 
	{
		if (chart != null) {			
			getRoot().remove(chart);
		}
		setLoading(true);
		
//		data.removeRows(0, data.getNumberOfRows());
		
		service.getBundeslandSiegerPartei(parteiBox.getItemText(parteiBox.getSelectedIndex()), new AsyncCallback<List<BundeslandSiegerPartei>>() {
			
			@Override
			public void onSuccess(List<BundeslandSiegerPartei> result) 
			{	
				//create datacontainer
				data = DataTable.create();
			    data.addColumn(ColumnType.STRING, "Bundesland");
			    data.addColumn(ColumnType.NUMBER, "Errungener Anteil in %");
			    
			    //create pie chart
			    options = Options.create();
			    options.setRegion("DE");
			    options.setColors(new int[]{BLANK_COLOR,getColor(parteiBox.getItemText(parteiBox.getSelectedIndex()))});
			    options.setDataMode(DataMode.REGIONS);
			    options.setShowLegend(true);
				
			    for(BundeslandSiegerPartei val : result)
			    {
			    	int index = data.addRow();
			    	
			    	data.setValue(index, 0, "DE-"+val.getBundesland());
			    	data.setValue(index, 1, val.getProzent());
			    }
				
				chart = new GeoMap(data, options);
			    
			    //GeoMap Bug
			    if(chart.getParent() == null)
			    	getRoot().add(chart);

			    chart.draw(data,options);
	
			    setLoading(false);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				getRoot().add(new Label(caught.getLocalizedMessage()));
				setLoading(false);
			}
		});
	}
}
