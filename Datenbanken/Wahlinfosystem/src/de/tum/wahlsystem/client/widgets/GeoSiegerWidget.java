package de.tum.wahlsystem.client.widgets;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.visualization.client.AbstractDataTable.ColumnType;
import com.google.gwt.visualization.client.DataTable;
import com.google.gwt.visualization.client.visualizations.GeoMap;
import com.google.gwt.visualization.client.visualizations.GeoMap.DataMode;
import com.google.gwt.visualization.client.visualizations.GeoMap.Options;

import de.tum.wahlsystem.client.WahlinfoServiceAsync;
import de.tum.wahlsystem.shared.BundeslandSieger;

public class GeoSiegerWidget extends AbstractWidget{

	DataTable data;
	
	GeoMap chart;
	Options options;
	
	private final static int CDU_COLOR = 0x000000;
	private final static int SPD_COLOR = 0xFF0000;
	private final static int LINKE_COLOR = 0x9932CC;
	
	private final static int MERGE_COLOR = 	0xCD6600;
	
	public GeoSiegerWidget(WahlinfoServiceAsync service) {
		super(service,"Geographisch I" ,"Geographische Ansicht Zweitstimmen");
	}

	@Override
	public void initialize() 
	{
		//create datacontainer
		data = DataTable.create();
	    data.addColumn(ColumnType.STRING, "Bundesland");
	    data.addColumn(ColumnType.NUMBER, "Farbe");
	    data.addColumn(ColumnType.STRING, "Siegerpartei");
	    
	    //create pie chart
	    options = Options.create();
	    options.setRegion("DE");
	    options.setColors(new int[]{CDU_COLOR,SPD_COLOR,LINKE_COLOR,MERGE_COLOR});
	    options.setDataMode(DataMode.REGIONS);
	    options.setShowLegend(false);
	    
	    
		chart = new GeoMap(data, options);
         
        //Load Data
        //load();
        //System.out.println("Inititliazed");
        //chart.draw(data, options);
        
	}
	
	public int getColorIndex(String partei)
	{	
		if(partei.equalsIgnoreCase("cdu"))
			return 0;
		if(partei.equalsIgnoreCase("csu"))
			return 0;
		
		if(partei.equalsIgnoreCase("spd"))
			return 1;
		
		if(partei.equalsIgnoreCase("die linke"))
			return 2;
		
		return 3;
	}

	@Override
	public void load() 
	{
		setLoading(true);
		
		data.removeRows(0, data.getNumberOfRows());
		
		service.getBundeslandSieger(new AsyncCallback<List<BundeslandSieger>>() {
			
			@Override
			public void onSuccess(List<BundeslandSieger> result) 
			{	
			    for(BundeslandSieger s : result)
			    {
			    	int index = data.addRow();
			    	
			    	data.setValue(index, 0, "DE-"+s.getBundesland());
			    	data.setValue(index,1,getColorIndex(s.getPartei()));
			    	data.setValue(index, 2, s.getPartei());
			    }
			    
			    //GeoMap Bug
			    if(chart.getParent() == null)
			    	getRoot().add(chart);

			    chart.draw(data,options);
	
			    setLoading(false);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				getRoot().add(new Label(caught.getLocalizedMessage()));
				setLoading(false);
			}
		});
	}
}
