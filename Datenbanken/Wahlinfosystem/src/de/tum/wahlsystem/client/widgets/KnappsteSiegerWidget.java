package de.tum.wahlsystem.client.widgets;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.visualization.client.AbstractDataTable.ColumnType;
import com.google.gwt.visualization.client.DataTable;
import com.google.gwt.visualization.client.visualizations.Table;
import com.google.gwt.visualization.client.visualizations.Table.Options;

import de.tum.wahlsystem.client.WahlinfoServiceAsync;
import de.tum.wahlsystem.shared.PolitikerErgebnis;

public class KnappsteSiegerWidget extends AbstractWidget {

	Table viz;
	DataTable data;
	Options options;
	
	public KnappsteSiegerWidget(WahlinfoServiceAsync service) {
		super(service, "Knappste Sieger","Knappste Sieger (Q6)");
	}

	@Override
	public void initialize() 
	{	
		viz = new Table();
        getRoot().add(viz);
        options = Table.Options.create();
        options.setShowRowNumber(true);
        
        data = DataTable.create();
	    data.addColumn(ColumnType.STRING, "Vorname");
	    data.addColumn(ColumnType.STRING, "Nachname");
	    data.addColumn(ColumnType.STRING, "Partei");
	    data.addColumn(ColumnType.NUMBER, "Abstand");
	    data.addColumn(ColumnType.BOOLEAN, "Sieger");
	    
	    viz.draw(data, options);
	    //load();
	}

	@Override
	public void load() {
		//Load data
		setLoading(true);
		data.removeRows(0, data.getNumberOfRows());
		
		service.getKnappste(new AsyncCallback<List<PolitikerErgebnis>>() {
			
			@Override
			public void onSuccess(List<PolitikerErgebnis> result) {
				    
			    	for(PolitikerErgebnis ps : result) {
			    		int index = data.addRow();
			    		
			    		data.setValue(index, 0, ps.getVorname());
			    		data.setValue(index, 1, ps.getNachname());
			    		data.setValue(index, 2, ps.getPartei());
			    		data.setValue(index, 3, ps.getStimmen());
			    		data.setValue(index, 4, ps.isSieger());
			    	}
		         
		         viz.draw(data, options);
		         setLoading(false);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				getRoot().add(new Label(caught.getLocalizedMessage()));
				setLoading(false);
			}
		});
		
	}

}