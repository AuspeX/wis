package de.tum.wahlsystem.client.widgets;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.visualization.client.DataTable;
import com.google.gwt.visualization.client.AbstractDataTable.ColumnType;
import com.google.gwt.visualization.client.visualizations.Table;
import com.google.gwt.visualization.client.visualizations.Table.Options;

import de.tum.wahlsystem.client.WahlinfoServiceAsync;
import de.tum.wahlsystem.shared.Politiker;

public class MitgliederBundestagWidget extends AbstractWidget {

	Table table;
	Options options;
	
	DataTable data;
	
	public MitgliederBundestagWidget(WahlinfoServiceAsync service) {
		super(service,"Bundestag","Mitglieder des Bundestags 2009 (Q2)");
	}

	@Override
	public void initialize() 
	{
		table = new Table();
		
		options = Table.Options.create();
        options.setShowRowNumber(true);
		
        getRoot().add(table);
        
        // Set up DataTable
        data = DataTable.create();
		data.addColumn(ColumnType.STRING, "Vorname");
		data.addColumn(ColumnType.STRING, "Nachname");
		data.addColumn(ColumnType.STRING, "Partei");
		
		table.draw(data, options);
		//Load data
		//load();
	}

	@Override
	public void load() 
	{
		setLoading(true);
		data.removeRows(0, data.getNumberOfRows());
		
		service.getSitzbelegung(new AsyncCallback<List<Politiker>>() {
			
			@Override
			public void onSuccess(List<Politiker> result) {
				    
				    for(Politiker p : result)
				    {
				    	int index = data.addRow();
				    	
				    	data.setValue(index, 0, p.getVorname());
				    	data.setValue(index, 1, p.getNachname());
				    	data.setValue(index, 2, p.getPartei());
				    }
		         
		         table.draw(data, options);
		         setLoading(false);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				getRoot().add(new Label(caught.getLocalizedMessage()));
				setLoading(false);
			}
		});
	}

}
