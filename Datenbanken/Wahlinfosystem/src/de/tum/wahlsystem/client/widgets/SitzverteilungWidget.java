package de.tum.wahlsystem.client.widgets;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.visualization.client.AbstractDataTable.ColumnType;
import com.google.gwt.visualization.client.DataTable;
import com.google.gwt.visualization.client.visualizations.Table;
import com.google.gwt.visualization.client.visualizations.corechart.PieChart;
import com.google.gwt.visualization.client.visualizations.corechart.PieChart.PieOptions;

import de.tum.wahlsystem.client.WahlinfoServiceAsync;
import de.tum.wahlsystem.shared.Sitz;

public class SitzverteilungWidget extends AbstractWidget{

	DataTable data;
	
	PieChart chart;
	PieOptions options;
	
	Table table;
	
	public SitzverteilungWidget(WahlinfoServiceAsync service) {
		super(service,"Sitzverteilung", "Sitzverteilung 2009 (Q1)");
	}

	@Override
	public void initialize() 
	{
		Grid g = new Grid(1,2);
		g.setWidth("100%");
		
		/* create datacontainer */
		data = DataTable.create();
	    data.addColumn(ColumnType.STRING, "Partei");
	    data.addColumn(ColumnType.NUMBER, "Sitze");
	    
	    /* create pie chart */
	    options = PieChart.createPieOptions();
	    options.setWidth(400);
	    options.setHeight(240);
	    options.set3D(true);
	    options.setTitle("Sitze des Bundestags");
	    
		chart = new PieChart(data, options);
	    g.setWidget(0, 0, chart);
	    
	    /* Create Table */
	    table = new Table(); 
        g.setWidget(0, 1, table);
        
        getRoot().add(g);
         
        /* Load Data */
        //load();
	}

	@Override
	public void load() 
	{
		setLoading(true);
		data.removeRows(0, data.getNumberOfRows());
		
		service.getSitzverteilung(new AsyncCallback<List<Sitz>>() {
			
			@Override
			public void onSuccess(List<Sitz> result) 
			{	
			    for(Sitz s : result)
			    {
			    	int index = data.addRow();
			    	
			    	data.setValue(index, 0, s.getName());
			    	data.setValue(index, 1, s.getNumber());
			    }
			    
			    table.draw(data);
			    chart.draw(data,options);
			    setLoading(false);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				getRoot().add(new Label(caught.getLocalizedMessage()));
				setLoading(false);
			}
		});
	}

}
