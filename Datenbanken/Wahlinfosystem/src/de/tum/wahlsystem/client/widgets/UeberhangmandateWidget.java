package de.tum.wahlsystem.client.widgets;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.visualization.client.DataTable;
import com.google.gwt.visualization.client.AbstractDataTable.ColumnType;
import com.google.gwt.visualization.client.visualizations.Table;
import com.google.gwt.visualization.client.visualizations.Table.Options;

import de.tum.wahlsystem.client.WahlinfoServiceAsync;
import de.tum.wahlsystem.shared.Ueberhang;

public class UeberhangmandateWidget extends AbstractWidget {

	Table table;
	Options options;
	
	DataTable data;
	
	public UeberhangmandateWidget(WahlinfoServiceAsync service) {
		super(service, "Überhangmandate","Überhangmandate 2009 (Q5)");
	}

	@Override
	public void initialize() 
	{
		table = new Table();
		
        options = Table.Options.create();
        options.setShowRowNumber(true);
        
        getRoot().add(table);
        
        data = DataTable.create();
		data.addColumn(ColumnType.STRING, "Bundesland");
		data.addColumn(ColumnType.STRING, "Partei");
		data.addColumn(ColumnType.NUMBER, "Überhangmandate");
		  
		table.draw(data, options);
		//load();
	}

	@Override
	public void load() {
		
		setLoading(true);
		data.removeRows(0, data.getNumberOfRows());
		
		service.getUeberhaenge(new AsyncCallback<List<Ueberhang>>()
				{
					@Override
					public void onSuccess(List<Ueberhang> result) {
						    
						    for(Ueberhang p : result)
						    {
						    	int index = data.addRow();
						    	
						    	data.setValue(index, 0, p.getBundesland());
						    	data.setValue(index, 1, p.getPartei());
						    	data.setValue(index, 2, p.getAnzahl());
						    }
				         
				         table.draw(data, options);
				         setLoading(false);
					}
					
					@Override
					public void onFailure(Throwable caught) {
						getRoot().add(new Label(caught.getLocalizedMessage()));
						setLoading(false);
					}
				});

	}

}
