package de.tum.wahlsystem.client.widgets;

import java.util.Random;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.visualization.client.DataTable;
import com.google.gwt.visualization.client.AbstractDataTable.ColumnType;
import com.google.gwt.visualization.client.visualizations.Table;

import de.tum.wahlsystem.client.WahlinfoServiceAsync;
import de.tum.wahlsystem.shared.ParteiErgebnis;
import de.tum.wahlsystem.shared.WahlkreisInfo;

public class WahlkreisWidget extends AbstractWidget implements ClickHandler{

	Random r;
	
	Button nextButton;
	
	DataTable dataWahlkreis;
	DataTable dataParteien;
	
	Table parteienInfo;
	Table wahlkreisInfo;
	
	NumberFormat fm = NumberFormat.getFormat("#0.00");
	
	public WahlkreisWidget(WahlinfoServiceAsync service) {
		super(service, "Wahlkreise","Wahlkreis Übersicht (Q3)");
	}

	@Override
	public void initialize() 
	{
		r = new Random();
		
		Grid g = new Grid(1,2);
		g.setWidth("100%");
		
		nextButton = new Button("Nächster Wahlkreis",this);
		getRoot().add(nextButton);
		
		dataWahlkreis = DataTable.create();
		dataWahlkreis.addColumn(ColumnType.STRING, "Attribut");
		dataWahlkreis.addColumn(ColumnType.STRING, "Wert");
		
		dataParteien = DataTable.create();
		dataParteien.addColumn(ColumnType.STRING, "Partei");
		dataParteien.addColumn(ColumnType.STRING, "Erststimmen 2009");
		dataParteien.addColumn(ColumnType.STRING, "Zweitstimmen 2009");
		dataParteien.addColumn(ColumnType.STRING, "Erststimmen 2005");
		dataParteien.addColumn(ColumnType.STRING, "Zweitstimmen 2005");
		
		wahlkreisInfo = new Table();
		wahlkreisInfo.setWidth("90%");
		
		parteienInfo = new Table();
		
		g.setWidget(0, 0, wahlkreisInfo);
		g.setWidget(0, 1, parteienInfo);
		
		g.getCellFormatter().getElement(0, 0).setAttribute("align", "left");
		g.getCellFormatter().getElement(0, 0).setAttribute("width", "40%");
		g.getCellFormatter().getElement(0, 0).setAttribute("valign", "top");
		
		g.getCellFormatter().getElement(0, 1).setAttribute("align", "right");
		g.getCellFormatter().getElement(0, 1).setAttribute("valign", "top");
		
		getRoot().add(g);
		
		parteienInfo.draw(dataParteien);
		wahlkreisInfo.draw(dataWahlkreis);
		//load();
	}

	@Override
	public void load() 
	{
		setLoading(true);
		dataParteien.removeRows(0, dataParteien.getNumberOfRows());
		dataWahlkreis.removeRows(0, dataWahlkreis.getNumberOfRows());
		
		
		//New Wahlkreis
		final int wahlkreisId = r.nextInt(299) + 1;
		
		service.getWahlkreisInfo(wahlkreisId, new AsyncCallback<WahlkreisInfo>()
				{
					
					@Override
					public void onSuccess(WahlkreisInfo result) 
					{
						/* Update Wahlkreis Info*/
						
						int index = dataWahlkreis.addRow();
						dataWahlkreis.setValue(index, 0, "Wahlkreis");
						dataWahlkreis.setValue(index, 1, "" + wahlkreisId);
						
						index = dataWahlkreis.addRow();
						dataWahlkreis.setValue(index, 0, "Name");
						dataWahlkreis.setValue(index, 1, result.getWahlkreisName());
						
						index = dataWahlkreis.addRow();
						dataWahlkreis.setValue(index, 0, "Beteiligung");
						dataWahlkreis.setValue(index, 1, result.getWahlBeteiligung() + "%");
						
						if(result.getDirektKandidat() != null)
						{
							index = dataWahlkreis.addRow();
							dataWahlkreis.setValue(index, 0, "Sieger");
							dataWahlkreis.setValue(index, 1, result.getDirektKandidat().getVorname()
									+" "+result.getDirektKandidat().getNachname() + " ("+result.getDirektKandidat().getPartei()+")");
						}
						
						int sumErststimmen2009 = result.getErststimmen2009Sum();
						int sumZweitstimmen2009 = result.getZweitstimmen2009Sum();
						
						for(ParteiErgebnis p : result.getParteien2009())
					    {
					    	index = dataParteien.addRow();
					    	
					    	dataParteien.setValue(index, 0, p.getParteiName());
					    	dataParteien.setValue(index, 1, ""+p.getAnzahlErststimmen()+" ("+ fm.format(((double)p.getAnzahlErststimmen()/sumErststimmen2009)*100.0) +")");
					    	dataParteien.setValue(index, 2, ""+p.getAnzahlZweitstimmen()+" ("+ fm.format(((double)p.getAnzahlErststimmen()/sumZweitstimmen2009)*100.0) +")");
					    }
						
						int sumErststimmen2005 = result.getErststimmen2005Sum();
						int sumZweitstimmen2005 = result.getZweitstimmen2005Sum();
						
						for(ParteiErgebnis p : result.getParteien2005())
					    {
							boolean found = false;
							for(int idx = 0; idx < dataParteien.getNumberOfRows(); idx++)
							{
								String parteiName = dataParteien.getValueString(idx, 0);
								
								if(parteiName.equals(p.getParteiName()))
								{
									//Found Value
									dataParteien.setValue(idx, 3, ""+p.getAnzahlErststimmen()+" ("+ fm.format(((double)p.getAnzahlErststimmen()/sumErststimmen2005)*100.0) +")");
							    	dataParteien.setValue(idx, 4, ""+p.getAnzahlZweitstimmen()+" ("+ fm.format(((double)p.getAnzahlErststimmen()/sumZweitstimmen2005)*100.0) +")");
									found = true;
								}
							}
							
							if(!found)
							{
							index = dataParteien.addRow();
					    	dataParteien.setValue(index, 0, p.getParteiName());
					    	dataParteien.setValue(index, 3, ""+p.getAnzahlErststimmen()+" ("+ fm.format(((double)p.getAnzahlErststimmen()/sumErststimmen2009)*100.0) +")");
					    	dataParteien.setValue(index, 4, ""+p.getAnzahlZweitstimmen()+" ("+ fm.format(((double)p.getAnzahlErststimmen()/sumZweitstimmen2009)*100.0) +")");
					    	}
					    }
						
						
						//Update GUI
						
						wahlkreisInfo.draw(dataWahlkreis);
						parteienInfo.draw(dataParteien);
						
						setLoading(false);
					}
					
					@Override
					public void onFailure(Throwable caught) {
						getRoot().add(new Label(caught.getLocalizedMessage()));
						setLoading(false);
					}
				});
	}

	@Override
	public void onClick(ClickEvent event) 
	{
		load();
	}

}
