package de.tum.wahlsystem.client.widgets;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.visualization.client.DataTable;
import com.google.gwt.visualization.client.AbstractDataTable.ColumnType;
import com.google.gwt.visualization.client.visualizations.Table;
import com.google.gwt.visualization.client.visualizations.Table.Options;

import de.tum.wahlsystem.client.WahlinfoServiceAsync;
import de.tum.wahlsystem.shared.WahlkreisSieger;

public class WahlkreissiegerWidget extends AbstractWidget {

	Table table;
	Options options;
	
	DataTable data;
	
	public WahlkreissiegerWidget(WahlinfoServiceAsync service) {
		super(service, "Wahlkreissieger","Wahlkreissieger 2009 (Q4)");
	}

	@Override
	public void initialize() 
	{
		table = new Table();
		
        options = Table.Options.create();
        options.setShowRowNumber(true);
        
        getRoot().add(table);
        
        data = DataTable.create();
		data.addColumn(ColumnType.NUMBER, "Wahlkreis");
		data.addColumn(ColumnType.STRING, "Erststimme_Sieger");
		data.addColumn(ColumnType.STRING, "Zweitstimme_Sieger");
		    
		table.draw(data, options);
		//load();
	}

	@Override
	public void load() {
		
		setLoading(true);
		data.removeRows(0, data.getNumberOfRows());
		
		service.getWahlkreisSieger(new AsyncCallback<List<WahlkreisSieger>>()
				{
					@Override
					public void onSuccess(List<WahlkreisSieger> result) {
						    
						    for(WahlkreisSieger w : result)
						    {
						    	int index = data.addRow();
						    	
						    	data.setValue(index, 0, w.getWahlkreis());
						    	data.setValue(index, 1, w.getErststimmeSieger());
						    	data.setValue(index, 2, w.getZweitstimmeSieger());
						    }
				         
				         table.draw(data, options);
				         setLoading(false);
					}
					
					@Override
					public void onFailure(Throwable caught) {
						getRoot().add(new Label(caught.getLocalizedMessage()));
						setLoading(false);
					}
				});

	}

}
