package de.tum.wahlsystem.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.postgresql.ds.PGPoolingDataSource;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import de.tum.wahlsystem.client.WahlinfoService;
import de.tum.wahlsystem.shared.BundeslandSieger;
import de.tum.wahlsystem.shared.BundeslandSiegerPartei;
import de.tum.wahlsystem.shared.ParteiErgebnis;
import de.tum.wahlsystem.shared.Politiker;
import de.tum.wahlsystem.shared.PolitikerErgebnis;
import de.tum.wahlsystem.shared.Sitz;
import de.tum.wahlsystem.shared.Ueberhang;
import de.tum.wahlsystem.shared.WahlkreisInfo;
import de.tum.wahlsystem.shared.WahlkreisSieger;

/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class WahlinfoServiceImpl extends RemoteServiceServlet implements
		WahlinfoService {

	PGPoolingDataSource dataSource;

	public WahlinfoServiceImpl()
	{
		super();

			try 
			{
				//setup datasource
				dataSource = new PGPoolingDataSource();

				dataSource.setDataSourceName("postgres_wahlsystem");
				dataSource.setServerName("localhost");
				dataSource.setPortNumber(5432);
				dataSource.setDatabaseName("elite");
				dataSource.setUser("postgres");
				dataSource.setPassword("postgres");
				dataSource.setMaxConnections(50);
			} catch (Exception e) {
				e.printStackTrace();
			}
	} 
	
	/**
	 * Returns the Sitzverteilung for 2009
	 * 
	 * @return List of Seats
	 */
	public List<Sitz> getSitzverteilung()
	{
		List<Sitz> seats = new ArrayList<Sitz>();
		
		Statement stmt = null;
		Connection con = null;
		ResultSet rs = null;
		
		try {
			con = dataSource.getConnection();
			stmt = con.createStatement();

			//Query
			rs = stmt.executeQuery("SELECT * FROM \"Result_Sitzverteilung_Partei_2009\"");

			//Add to result
		    while (rs.next()) 
		    	seats.add(new Sitz(rs.getString(1),rs.getInt(2)));
		    
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			if (rs != null)
				try { rs.close(); } catch (SQLException e) {}
			if (stmt != null)
				try { stmt.close(); } catch (SQLException e) {}
			if (con != null)
		        try { con.close(); } catch (SQLException e) {}
		}

		//Return result
		return seats;
	}
	
	/**
	 * 
	 * Returns the Politiker who got a seat
	 * 
	 * @return list of Politiker
	 */
	public List<Politiker> getSitzbelegung()
	{
		List<Politiker> politicians = new ArrayList<Politiker>();

		Statement stmt = null;
		Connection con = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			stmt = con.createStatement();

			//Query
			rs = stmt.executeQuery("SELECT * FROM \"Result_Sitzverteilung_2009\"");

			//Add to result
		    while (rs.next()) 
		    	politicians.add(new Politiker(rs.getString(2),rs.getString(3),rs.getString(4)));
		
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			if (rs != null)
				try { rs.close(); } catch (SQLException e) {}
			if (stmt != null)
				try { stmt.close(); } catch (SQLException e) {}
			if (con != null)
		        try { con.close(); } catch (SQLException e) {} 
		}

		return politicians;
	}
	
	/**
	 * 
	 * Returns the Ueberhaenge by Partei and Bundesland
	 * 
	 * @return List of Ueberhaenge
	 */
	public List<Ueberhang> getUeberhaenge() 
	{
		List<Ueberhang> ueberhang = new ArrayList<Ueberhang>();

		Statement stmt = null;
		Connection con = null;
		ResultSet rs = null;
		
		try {
			con = dataSource.getConnection();
			stmt = con.createStatement();

			//Query
			rs = stmt.executeQuery("SELECT * FROM \"Result_Uberhang_Bundeslaender_2009\"");

			//Add to result
		    while (rs.next()) 
		    	ueberhang.add(new Ueberhang(rs.getString(1),rs.getString(2),rs.getInt(3)));
		    
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			if (rs != null)
				try { rs.close(); } catch (SQLException e) {}
			if (stmt != null)
				try { stmt.close(); } catch (SQLException e) {}
			if (con != null)
		        try { con.close(); } catch (SQLException e) {}
		}
		
		//Return result
		return ueberhang;
	}
	
	

	/**
	 * 
	 * Gets information about the given Wahlkreis
	 * 
	 * @return WahlkreisInfo
	 */
	public WahlkreisInfo getWahlkreisInfo(int wahlkreisId) 
	{
		WahlkreisInfo info = new WahlkreisInfo();
		info.setWahlkreisId(wahlkreisId);
		
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		//Read general Info
		try {
			con = dataSource.getConnection();
			stmt = con.prepareStatement(
					"select wahlkreis_id, wahlkreis_name, (waehler_2009/cast(wahlkreis_berechtigte_2009 as decimal(10,2)))*100.0 as wahlbeteiligung " +
					"from \"Wahlkreise\" INNER JOIN " +
					"	(select stimme_wahlkreis,sum(stimme_anzahl) as waehler_2009 " +
					"	from \"Erststimmen\" " +
					"	where stimme_wahl = 2009 " +
					"	group by stimme_wahlkreis) as beteiligung " +
					"ON stimme_wahlkreis = wahlkreis_id " +
					"WHERE wahlkreis_id = ?"
				);
			
			stmt.setInt(1, wahlkreisId);
			
			//Query
			rs = stmt.executeQuery();
		    
			//Read data
		    while (rs.next()) 
		    {
		    	info.setWahlkreisName(rs.getString(2));
		    	info.setWahlBeteiligung(rs.getDouble(3));
		    }
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			if (rs != null)
				try { rs.close(); } catch (SQLException e) {}
			if (stmt != null)
				try { stmt.close(); } catch (SQLException e) {}
			if (con != null)
		        try { con.close(); } catch (SQLException e) {}
		}
		
		//Read Direktkandidat
		try {
			con = dataSource.getConnection();
			stmt = con.prepareStatement("SELECT * FROM \"Result_Direktkandidaten_Gesamt_2009\" where direktkandidat_wahlkreis = ?");
			stmt.setInt(1, wahlkreisId);
			
			//Query
			rs = stmt.executeQuery();
		    
			//Add to result
		    while (rs.next()) 
		    	info.setDirektKandidat(new Politiker(rs.getString(3),rs.getString(4),rs.getString(5)));
		    
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			if (rs != null)
				try { rs.close(); } catch (SQLException e) {}
			if (stmt != null)
				try { stmt.close(); } catch (SQLException e) {}
			if (con != null)
		        try { con.close(); } catch (SQLException e) {}
		}
		
		
		List<ParteiErgebnis> erg2009 = new ArrayList<ParteiErgebnis>();
		//Read Partei 2009
		try {
			con = dataSource.getConnection();
			stmt = con.prepareStatement("select partei,erststimmen,zweitstimmen from \"Result_Parteien_Wahlkreis\" where wahlkreis = ? and wahl = ?");
			
			stmt.setInt(1, wahlkreisId);
			stmt.setInt(2, 2009);
			
			//Query
			rs = stmt.executeQuery();
		    
			//Add to result
		    while (rs.next()) 
		    {
		    	ParteiErgebnis erg = new ParteiErgebnis();
		    	erg.setParteiName(rs.getString(1));
		    	erg.setAnzahlErststimmen(rs.getInt(2));
		    	erg.setAnzahlZweitstimmen(rs.getInt(3));
		    	
		    	erg2009.add(erg);
		    }
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			if (rs != null)
				try { rs.close(); } catch (SQLException e) {}
			if (stmt != null)
				try { stmt.close(); } catch (SQLException e) {}
			if (con != null)
		        try { con.close(); } catch (SQLException e) {}
		}
		info.setParteien2009(erg2009);
		
		List<ParteiErgebnis> erg2005 = new ArrayList<ParteiErgebnis>();
		//Read Partei 2005
		try {
			con = dataSource.getConnection();
			stmt = con.prepareStatement("select partei,erststimmen,zweitstimmen from \"Result_Parteien_Wahlkreis\" where wahlkreis = ? and wahl = ?");
			
			stmt.setInt(1, wahlkreisId);
			stmt.setInt(2, 2005);
			
			//Query
			rs = stmt.executeQuery();
		    
			//Add to result
		    while (rs.next()) 
		    {
		    	ParteiErgebnis erg = new ParteiErgebnis();
		    	erg.setParteiName(rs.getString(1));
		    	erg.setAnzahlErststimmen(rs.getInt(2));
		    	erg.setAnzahlZweitstimmen(rs.getInt(3));
		    	
		    	erg2005.add(erg);
		    }
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			if (rs != null)
				try { rs.close(); } catch (SQLException e) {}
			if (stmt != null)
				try { stmt.close(); } catch (SQLException e) {}
			if (con != null)
		        try { con.close(); } catch (SQLException e) {}
		}
		info.setParteien2005(erg2005);
		
		//Return collected infos
		return info;
	}

	/**
	 * 
	 * Collects the winner of each Wahlkreis
	 * 
	 * @return List of WahlkreisSieger
	 */
	public List<WahlkreisSieger> getWahlkreisSieger() {
		List<WahlkreisSieger> sieger = new ArrayList<WahlkreisSieger>();
		
		Statement stmt = null;
		Connection con = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			stmt = con.createStatement();
			
			//Query
			rs = stmt.executeQuery(
					"Select wahlkreis, erststimme_partei,landesliste_partei as zweitstimme_partei " +
					"from \"Result_Wahlkreis_Sieger_Erststimme\" e " +
					"INNER JOIN \"Result_Wahlkreis_Sieger_Zweitstimme\" z " +
					"ON e.wahlkreis = z.stimme_wahlkreis "+ 
					"order by wahlkreis");
		    
			//Add to result
		    while (rs.next()) 
		    	sieger.add(new WahlkreisSieger(rs.getInt(1),rs.getString(2),rs.getString(3)));
		    
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			if (rs != null)
				try { rs.close(); } catch (SQLException e) {}
			if (stmt != null)
				try { stmt.close(); } catch (SQLException e) {}
			if (con != null)
		        try { con.close(); } catch (SQLException e) {}
		}
		
		//return result
		return sieger;
	}
	
	
	/**
	 * 
	 * Finds the winner with the smallest distance to second
	 * 
	 * @return List of PolitikerErgebnis
	 */
	public List<PolitikerErgebnis> getKnappste() {
		List<PolitikerErgebnis> result = new ArrayList<PolitikerErgebnis>();

		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		
		try {
			con = dataSource.getConnection();
			stmt = con.createStatement();
			
			//Query
			rs = stmt.executeQuery("SELECT * FROM \"Result_Top10_Sieger_2009\"");
			
			//Add to result
			while(rs.next()) {
				PolitikerErgebnis ps = new PolitikerErgebnis();
				ps.setVorname(rs.getString(3));
				ps.setNachname(rs.getString(4));
				ps.setPartei(rs.getString(5));
				ps.setStimmen(rs.getInt(2));
				
				if (ps.getStimmen() > 20000000) {
					ps.setSieger(false);
					ps.setStimmen(ps.getStimmen()-20000000);
				} else {
					ps.setSieger(true);
				}
				
				result.add(ps);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			if (rs != null)
				try { rs.close(); } catch (SQLException e) {}
			if (stmt != null)
				try { stmt.close(); } catch (SQLException e) {}
			if (con != null)
		        try { con.close(); } catch (SQLException e) {}
		}

		return result;
	}
	
	/**
	 * Returns the WahlkreisInfo directly by Stimmzettel
	 * 
	 * @return WahlkreisInfo
	 */
	public WahlkreisInfo getWahlkreisInfoStimmzettel(int wahlkreisId) {
		{
			WahlkreisInfo info = new WahlkreisInfo();
			info.setWahlkreisId(wahlkreisId);
			
			//Read Wahlkreisinfo
			PreparedStatement stmt = null;
			Connection con = null;
			ResultSet rs = null;
			
			//Get general infos
			try {
				con = dataSource.getConnection();

				stmt = con.prepareStatement(
						"select wahlkreis_id, wahlkreis_name, (waehler_2009/cast(wahlkreis_berechtigte_2009 as decimal(10,2)))*100.0 as wahlbeteiligung " +
						"from \"Wahlkreise\" INNER JOIN " +
						"	(select stimme_wahlkreis,sum(stimme_anzahl) as waehler_2009 " +
						"	from \"Erststimmen_Stimmzettel\" " +
						"	where stimme_wahl = 2009 " +
						"	group by stimme_wahlkreis) as beteiligung " +
						"ON stimme_wahlkreis = wahlkreis_id " +
						"WHERE wahlkreis_id = ?"
					);
				
				stmt.setInt(1, wahlkreisId);
				
				//Query
				rs = stmt.executeQuery();
			    
				//Add to result
			    while (rs.next()) 
			    {
			    	info.setWahlkreisName(rs.getString(2));
			    	info.setWahlBeteiligung(rs.getDouble(3));
			    }
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			//Read Direktkandidat
			try {
				stmt = con.prepareStatement("SELECT * FROM \"Result_Direktkandidaten_Gesamt_2009_Stimmzettel\" where direktkandidat_wahlkreis = ?");
				stmt.setInt(1, wahlkreisId);
				
				//Query
				rs = stmt.executeQuery();
			    
				//Add to result
			    while (rs.next()) 
			    	info.setDirektKandidat(new Politiker(rs.getString(3),rs.getString(4),rs.getString(5)));
			    
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			
			List<ParteiErgebnis> erg2009 = new ArrayList<ParteiErgebnis>();
			//Read Parteien 2009
			try {
				stmt = con.prepareStatement("select partei,erststimmen,zweitstimmen from \"Result_Parteien_Wahlkreis_Stimmzettel\" where wahlkreis = ? and wahl = ?");
				
				stmt.setInt(1, wahlkreisId);
				stmt.setInt(2, 2009);
				
				//Query
				rs = stmt.executeQuery();
			    
				//Add to result
			    while (rs.next()) 
			    {
			    	ParteiErgebnis erg = new ParteiErgebnis();
			    	erg.setParteiName(rs.getString(1));
			    	erg.setAnzahlErststimmen(rs.getInt(2));
			    	erg.setAnzahlZweitstimmen(rs.getInt(3));
			    	
			    	erg2009.add(erg);
			    }
			} catch (SQLException e) {
				e.printStackTrace();
			}
			info.setParteien2009(erg2009);
			
			List<ParteiErgebnis> erg2005 = new ArrayList<ParteiErgebnis>();
			//Read Parteien 2005
			try {
				stmt = con.prepareStatement("select partei,erststimmen,zweitstimmen from \"Result_Parteien_Wahlkreis_Stimmzettel\" where wahlkreis = ? and wahl = ?");
				
				stmt.setInt(1, wahlkreisId);
				stmt.setInt(2, 2005);
				
				//Query
				rs = stmt.executeQuery();
			    
				//Add to result
			    while (rs.next()) 
			    {
			    	ParteiErgebnis erg = new ParteiErgebnis();
			    	erg.setParteiName(rs.getString(1));
			    	erg.setAnzahlErststimmen(rs.getInt(2));
			    	erg.setAnzahlZweitstimmen(rs.getInt(3));
			    	
			    	erg2005.add(erg);
			    }
			} catch (SQLException e) {
				e.printStackTrace();
			}
			info.setParteien2005(erg2005);
			
			return info;
		}
	}

	@Override
	public List<BundeslandSieger> getBundeslandSieger() {
		List<BundeslandSieger> sieger = new ArrayList<BundeslandSieger>();
		
		Statement stmt = null;
		Connection con = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			stmt = con.createStatement();
			
			//Query
			rs = stmt.executeQuery(
					"with \"Siege_Per_Bundesland\" as "+
					"(select count(*) as anz, bundesland_kz, landesliste_partei from \"Result_Wahlkreis_Sieger_Zweitstimme\", \"Wahlkreise\", \"Bundeslaender\" "+
					"where stimme_wahlkreis = wahlkreis_id and wahlkreis_bundesland = bundesland_id "+
					"group by landesliste_partei, bundesland_kz " +
					"order by bundesland_kz, anz desc) " +

					"select string_agg(landesliste_partei, ', ')  as partei, a.bundesland_kz as bundesland from \"Siege_Per_Bundesland\" a where anz >= (select max(anz) from \"Siege_Per_Bundesland\" b "+ 
					"where b.bundesland_kz = a.bundesland_kz group by bundesland_kz) "+
					"group by a.bundesland_kz "
					);
		    
			//Add to result
		    while (rs.next()) 
		    	sieger.add(new BundeslandSieger(rs.getString(1),rs.getString(2)));
		    
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			if (rs != null)
				try { rs.close(); } catch (SQLException e) {}
			if (stmt != null)
				try { stmt.close(); } catch (SQLException e) {}
			if (con != null)
		        try { con.close(); } catch (SQLException e) {}
		}
		
		return sieger;
	}

	@Override
	public List<BundeslandSiegerPartei> getBundeslandSiegerPartei(String partei) {
		List<BundeslandSiegerPartei> parteien = new ArrayList<BundeslandSiegerPartei>();
		
		PreparedStatement stmt = null;
		Connection con = null;
		ResultSet rs = null;
		
		partei = partei.toUpperCase();
		
		try {
			con = dataSource.getConnection();
			stmt = con.prepareStatement("SELECT stimmen_erhalten,total,a.bundesland_kz FROM \"Result_Siege_Partei_Per_Bundesland\" a, \"Result_Stimmen_Gesamt_Per_Bundesland\" b WHERE a.bundesland_kz = b.bundesland_kz AND mitglied_partei = ?");
			stmt.setString(1, partei);
			
			//Query
			rs = stmt.executeQuery();
			
		    
			//Add to result
		    while (rs.next()) 
		    	parteien.add(new BundeslandSiegerPartei(partei,rs.getString(3),rs.getInt(1),rs.getInt(2)));
		    
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			if (rs != null)
				try { rs.close(); } catch (SQLException e) {}
			if (stmt != null)
				try { stmt.close(); } catch (SQLException e) {}
			if (con != null)
		        try { con.close(); } catch (SQLException e) {}
		}
		
		return parteien;
	}
}
