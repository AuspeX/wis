package de.tum.wahlsystem.shared;

import java.io.Serializable;

public class BundeslandSieger implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1698145258630750206L;
	
	String partei;
	String bundesland;
	
	public BundeslandSieger() {
		super();
	}

	public BundeslandSieger(String partei, String bundesland) {
		super();
		this.partei = partei;
		this.bundesland = bundesland;
	}

	public String getPartei() {
		return partei;
	}

	public void setPartei(String partei) {
		this.partei = partei;
	}

	public String getBundesland() {
		return bundesland;
	}

	public void setBundesland(String bundesland) {
		this.bundesland = bundesland;
	}
	
}
