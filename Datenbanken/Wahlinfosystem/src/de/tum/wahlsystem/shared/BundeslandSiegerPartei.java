package de.tum.wahlsystem.shared;

import java.io.Serializable;

public class BundeslandSiegerPartei implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1698145258630750206L;
	
	String partei;
	String bundesland;
	double prozent;
	int total;
	int erhalten;
	
	public BundeslandSiegerPartei() {
		super();
	}

	public BundeslandSiegerPartei(String partei, String bundesland, int erhalten, int total) {
		super();
		this.partei = partei;
		this.prozent = (erhalten*100)/total;
		this.total = total;
		this.erhalten = erhalten;
		this.bundesland = bundesland;
	}
	
	public double getProzent() {
		return prozent;
	}
	
	public void setProzent(double prozent) {
		this.prozent = prozent;
	}

	public String getPartei() {
		return partei;
	}

	public void setPartei(String partei) {
		this.partei = partei;
	}

	public String getBundesland() {
		return bundesland;
	}

	public void setBundesland(String bundesland) {
		this.bundesland = bundesland;
	}
	
}
