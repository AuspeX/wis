package de.tum.wahlsystem.shared;


/**
 * This class represents a Partei
 * 
 * @author kaelumania
 *
 */
public class Partei {
	
	String name;
	String kuerzel;
	
	public Partei(String name, String kuerzel) {
		this.name = name;
		this.kuerzel = kuerzel;
	}

	public Partei(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getKuerzel() {
		return kuerzel;
	}

	public void setKuerzel(String kuerzel) {
		this.kuerzel = kuerzel;
	}
}
