package de.tum.wahlsystem.shared;

import java.io.Serializable;

/**
 * This class represents the result of a partei
 * 
 * @author kaelumania
 *
 */

public class ParteiErgebnis implements Serializable{
	
	private static final long serialVersionUID = -1970948083940717854L;

	String parteiName;
	
	int anzahlZweitstimmen;
	int anzahlErststimmen;
	
	public ParteiErgebnis(){super();}

	public String getParteiName() {
		return parteiName;
	}

	public void setParteiName(String parteiName) {
		this.parteiName = parteiName;
	}

	public int getAnzahlZweitstimmen() {
		return anzahlZweitstimmen;
	}

	public void setAnzahlZweitstimmen(int anzahlZweitstimmen) {
		this.anzahlZweitstimmen = anzahlZweitstimmen;
	}

	public int getAnzahlErststimmen() {
		return anzahlErststimmen;
	}

	public void setAnzahlErststimmen(int anzahlErststimmen) {
		this.anzahlErststimmen = anzahlErststimmen;
	}
	
}
