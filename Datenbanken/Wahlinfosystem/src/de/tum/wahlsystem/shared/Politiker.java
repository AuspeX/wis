package de.tum.wahlsystem.shared;

import java.io.Serializable;

/**
 * This class represents a Politiker
 * 
 * @author kaelumania
 *
 */

public class Politiker implements Serializable{

	private static final long serialVersionUID = -8104665546149066119L;
	
	String vorname;
	String nachname;
	
	String partei;

	public Politiker() {
		super();
	}

	public Politiker(String firstName, String lastName, String partyName) {
		super();
		this.vorname = firstName;
		this.nachname = lastName;
		this.partei = partyName;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getNachname() {
		return nachname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	public String getPartei() {
		return partei;
	}

	public void setPartei(String partei) {
		this.partei = partei;
	}
	
}
