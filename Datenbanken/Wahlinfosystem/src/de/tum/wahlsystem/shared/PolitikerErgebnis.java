package de.tum.wahlsystem.shared;

/**
 * This class presents the result of a Politiker
 * @author kaelumania
 *
 */
public class PolitikerErgebnis extends Politiker implements Comparable<PolitikerErgebnis>{

	private static final long serialVersionUID = 5418887669047160239L;
	
	private int stimmen;
	private int position;
	private boolean sieger = false;
	
	public int getStimmen() {
		return stimmen;
	}
	
	public void setStimmen(int score) {
		this.stimmen = score;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int rank) {
		this.position = rank;
	}

	public boolean isSieger() {
		return sieger;
	}

	public void setSieger(boolean winner) {
		this.sieger = winner;
	}

	@Override
	public int compareTo(PolitikerErgebnis o) {
//		if (this.isWinner() && !o.isWinner()) {
//			return -1;
//		} else if (!this.isWinner() && o.isWinner()) {
//			return 1;
//		} else {
			if (this.getPosition() < o.getPosition()) {
				return -1;
			} else if (this.getPosition() > o.getPosition()) {
				return 1;
			} else {
				return 0;
			}
//		}
	}

}