package de.tum.wahlsystem.shared;

import java.io.Serializable;

/**
 * This class represents a seat in the parliament
 * 
 * @author kaelumania
 *
 */

public class Sitz implements Serializable{

	private static final long serialVersionUID = -5478693760096471266L;
	
	private String name;
	private int number;
	
	public Sitz()
	{
		name = "Unknown";
		number = -1;
	}
	
	public Sitz(String name, int number)
	{
		super();
		
		this.name = name;
		this.number = number;
	}

	public String getName() {
		return name;
	}

	public int getNumber() {
		return number;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setNumber(int number) {
		this.number = number;
	}
	
	
	
}
