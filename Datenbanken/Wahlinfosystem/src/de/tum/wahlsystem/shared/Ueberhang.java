package de.tum.wahlsystem.shared;

import java.io.Serializable;

/**
 * This class represents a "Überhangmandat"
 * 
 * @author kaelumania
 *
 */

public class Ueberhang implements Serializable{

	private static final long serialVersionUID = -5753046993791347430L;
	
	String partei;
	String Bundesland;
	int anzahl;
	
	public Ueberhang() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Ueberhang(String partei, String bundesland, int anzahl) {
		super();
		this.partei = partei;
		Bundesland = bundesland;
		this.anzahl = anzahl;
	}



	public String getPartei() {
		return partei;
	}
	public void setPartei(String partei) {
		this.partei = partei;
	}
	public String getBundesland() {
		return Bundesland;
	}
	public void setBundesland(String bundesland) {
		Bundesland = bundesland;
	}
	public int getAnzahl() {
		return anzahl;
	}
	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}
	
	
	
}
