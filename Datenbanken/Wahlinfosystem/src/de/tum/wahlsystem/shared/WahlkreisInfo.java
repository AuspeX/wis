package de.tum.wahlsystem.shared;

import java.io.Serializable;
import java.util.List;

/**
 * This class represents information about a Wahlkreis
 * 
 * @author kaelumania
 *
 */

public class WahlkreisInfo implements Serializable{

	
	private static final long serialVersionUID = -7404705515712437024L;
	
	int wahlkreisId;
	String wahlkreisName;
	double wahlBeteiligung;
	
	Politiker direktKandidat;
	
	List<ParteiErgebnis> parteien2009;
	List<ParteiErgebnis> parteien2005;
	

	public int getWahlkreisId() {
		return wahlkreisId;
	}
	public void setWahlkreisId(int wahlkreisId) {
		this.wahlkreisId = wahlkreisId;
	}
	public String getWahlkreisName() {
		return wahlkreisName;
	}
	public void setWahlkreisName(String wahlkreisName) {
		this.wahlkreisName = wahlkreisName;
	}
	public double getWahlBeteiligung() {
		return wahlBeteiligung;
	}
	public void setWahlBeteiligung(double wahlBeteiligung) {
		this.wahlBeteiligung = wahlBeteiligung;
	}
	public Politiker getDirektKandidat() {
		return direktKandidat;
	}
	public void setDirektKandidat(Politiker direktKandidat) {
		this.direktKandidat = direktKandidat;
	}
	public List<ParteiErgebnis> getParteien2009() {
		return parteien2009;
	}
	public void setParteien2009(List<ParteiErgebnis> parteien2009) {
		this.parteien2009 = parteien2009;
	}
	public List<ParteiErgebnis> getParteien2005() {
		return parteien2005;
	}
	public void setParteien2005(List<ParteiErgebnis> parteien2005) {
		this.parteien2005 = parteien2005;
	}
	
	public int getErststimmen2009Sum(){
		int result = 0;
		
		for(ParteiErgebnis p : parteien2009)
			result += p.getAnzahlErststimmen();
			
		return result;
	}
	public int getZweitstimmen2009Sum(){
		int result = 0;
		
		for(ParteiErgebnis p : parteien2009)
			result += p.getAnzahlZweitstimmen();
			
		return result;
	}
	
	public int getErststimmen2005Sum(){
		int result = 0;
		
		for(ParteiErgebnis p : parteien2005)
			result += p.getAnzahlErststimmen();
			
		return result;
	}
	public int getZweitstimmen2005Sum(){
		int result = 0;
		
		for(ParteiErgebnis p : parteien2005)
			result += p.getAnzahlZweitstimmen();
			
		return result;
	}
	
}
