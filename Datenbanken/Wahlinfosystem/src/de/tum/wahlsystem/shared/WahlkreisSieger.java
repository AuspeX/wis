package de.tum.wahlsystem.shared;

import java.io.Serializable;

/**
 * This class represents a result in a given Wahlkreis
 * 
 * @author kaelumania
 *
 */

public class WahlkreisSieger implements Serializable {

	private static final long serialVersionUID = -8748526465283532023L;

	int wahlkreis;
	
	String erststimmeSieger;
	String zweitstimmeSieger;
	
	
	
	public WahlkreisSieger() {
		super();
	}


	public WahlkreisSieger(int wahlkreis, String erststimmeSieger,
			String zweitstimmeSieger) {
		super();
		this.wahlkreis = wahlkreis;
		this.erststimmeSieger = erststimmeSieger;
		this.zweitstimmeSieger = zweitstimmeSieger;
	}


	public int getWahlkreis() {
		return wahlkreis;
	}


	public void setWahlkreis(int wahlkreis) {
		this.wahlkreis = wahlkreis;
	}


	public String getErststimmeSieger() {
		return erststimmeSieger;
	}


	public void setErststimmeSieger(String erststimmeSieger) {
		this.erststimmeSieger = erststimmeSieger;
	}


	public String getZweitstimmeSieger() {
		return zweitstimmeSieger;
	}


	public void setZweitstimmeSieger(String zweitstimmeSieger) {
		this.zweitstimmeSieger = zweitstimmeSieger;
	}
	
	
	
}
