# coding: utf8

import sys
import string

listeKZ = ("spd","cdu","fdp","linke","gruene","csu","npd","rep","familie","tierschutz","pbc","mlpd","bueso","bp","psg","volksabstimmung","volksabstimmung","zentrum","adm","cm","dkp","dvu","violetten","fwd","oedp","piraten","rrp","rentner","freieunion")
listeName = ("SPD","CDU","FDP","DIE LINKE","GRÜNE","CSU","NPD","REP","FAMILIE","Die Tierschutzpartei","PBC","MLPD","BüSo","BP","PSG","Volksabst.","Deutschland","ZENTRUM","ADM","CM","DKP","DVU","DIE VIOLETTEN","FWD","ödp","PIRATEN","RRP","RENTNER","Freie Union")
jahre = (2005,2009)

output = open("/Users/kaelumania/Desktop/erststimmen.sql",'w')

for jahr in jahre:
	i = 0
	for kz in listeKZ:
		sql = """SELECT COALESCE(erststimmen_"""+kz.lower()+"""_"""+str(jahr)+""", 0) AS anzahl, true AS gueltig, wahlkreis_bundesland AS wahlkreis, direktkandidat_id, """+str(jahr)+""" AS jahr
				FROM "csv_Ergebnis"
				LEFT JOIN 
				(
				Select * from "Direktkandidaten" 
				JOIN "Politiker" ON direktkandidat_politiker = politiker_id 
				JOIN "Mitglieder" ON mitglied_jahr = """+str(jahr)+""" AND politiker_id = mitglied_politiker AND lower(mitglied_partei) = lower('"""+listeName[i]+"""')
				) as t ON wahlkreis_bundesland = direktkandidat_wahlkreis AND direktkandidat_wahl = """+str(jahr)+"""
				WHERE "csv_Ergebnis".bundesland <> 99 AND "csv_Ergebnis".bundesland IS NOT NULL
				union all
				"""
		output.write(sql)
		i = i + 1

for jahr in jahre:
	uebrige = """SELECT COALESCE(erststimmen_uebrige_"""+str(jahr)+""", 0) AS anzahl, true AS gueltig, wahlkreis_bundesland AS wahlkreis, null as direktkandidat_id, """+str(jahr)+""" AS jahr
				FROM "csv_Ergebnis"
				WHERE "csv_Ergebnis".bundesland <> 99 AND "csv_Ergebnis".bundesland IS NOT NULL
				union all
				"""
	output.write(uebrige)

for jahr in jahre:
	ungueltige = """SELECT COALESCE(erststimmen_"""+str(jahr)+"""_ungueltig, 0) AS anzahl, false AS gueltig, wahlkreis_bundesland AS wahlkreis, null as direktkandidat_id, """+str(jahr)+""" AS jahr
				FROM "csv_Ergebnis"
				WHERE "csv_Ergebnis".bundesland <> 99 AND "csv_Ergebnis".bundesland IS NOT NULL
				union all
				"""
	output.write(ungueltige)

output.close()