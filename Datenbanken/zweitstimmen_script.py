# coding: utf8

import sys
import string

listeKZ = ("spd","cdu","fdp","linke","gruene","csu","npd","rep","familie","tierschutz","pbc","mlpd","bueso","bp","psg","volksabstimmung","volksabstimmung","zentrum","adm","cm","dkp","dvu","violetten","fwd","oedp","piraten","rrp","rentner","freieunion")
listeName = ("SPD","CDU","FDP","DIE LINKE","GRÜNE","CSU","NPD","REP","FAMILIE","Die Tierschutzpartei","PBC","MLPD","BüSo","BP","PSG","Volksabst.","Deutschland","ZENTRUM","ADM","CM","DKP","DVU","DIE VIOLETTEN","FWD","ödp","PIRATEN","RRP","RENTNER","Freie Union")
jahre = (2005,2009)

output = open("/Users/kaelumania/Desktop/zweitstimmen.sql",'w')

for jahr in jahre:
	i = 0
	for kz in listeKZ:
		sql = """
		select COALESCE(zweitstimmen_"""+kz.lower()+"""_"""+str(jahr)+""",0) as anzahl,true as gueltig,wahlkreis_bundesland as wahlkreis,landesliste_id as landesliste, """+str(jahr)+""" as jahr
		from \"csv_Ergebnis\" inner join \"Landeslisten\"
			ON lower(landesliste_partei) = lower('"""+listeName[i]+"""')
			AND bundesland = landesliste_bundesland
			AND landesliste_jahr = """+str(jahr)+"""
		where bundesland != 99 and bundesland is not null
		union all"""
		output.write(sql)
		i = i + 1

for jahr in jahre:
	uebrige = """
		select COALESCE(zweitstimmen_uebrige_"""+str(jahr)+""",0) as anzahl, true as gueltig, wahlkreis_bundesland as wahlkreis, null as landesliste, """+str(jahr)+""" as jahr 
		from \"csv_Ergebnis\"
		where bundesland != 99 and bundesland is not null
		union all"""
	output.write(uebrige)

for jahr in jahre:
	ungueltige = """
		select COALESCE(zweitstimmen_"""+str(jahr)+"""_ungueltig,0) as anzahl, false as gueltig, wahlkreis_bundesland as wahlkreis, null as landesliste, """+str(jahr)+""" as jahr 
		from \"csv_Ergebnis\"
		where bundesland != 99 and bundesland is not null
		union all"""
	output.write(ungueltige)

output.close()