
public class Erststimme extends Stimme{
	
	private long direktkandidat;

	public long getDirektkandidat() {
		return direktkandidat;
	}

	public void setDirektkandidat(long direktkandidat) {
		this.direktkandidat = direktkandidat;
	}

	@Override
	public String toString() {
		return "[stimme_anzahl="+getAnzahl()+",stimme_wahlkreis="+getWahlkreis()+",direktkandidat="+getDirektkandidat()+"]";
	}

}
