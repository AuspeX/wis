
public class Pollsheet {
	
	private String hash;
	private long timestamp;
	private long erststimme;
	private long zweitstimme;
	private boolean gueltig;
	private long bezirk;
	
	public Pollsheet(int year) {
//		this.timestamp = Utils.randomTimestamp(year);
		//TODO - generate hash
		this.hash = "N/A";
	}
	
	public String getHash() {
		return hash;
	}
	
	public void setHash(String hash) {
		this.hash = hash;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public long getErststimme() {
		return erststimme;
	}

	public void setErststimme(long erststimme) {
		this.erststimme = erststimme;
	}

	public long getZweitstimme() {
		return zweitstimme;
	}

	public void setZweitstimme(long zweitstimme) {
		this.zweitstimme = zweitstimme;
	}

	public boolean isGueltig() {
		return gueltig;
	}

	public void setGueltig(boolean gueltig) {
		this.gueltig = gueltig;
	}

	public long getBezirk() {
		return bezirk;
	}

	public void setBezirk(long bezirk) {
		this.bezirk = bezirk;
	}

}