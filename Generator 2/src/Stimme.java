
public abstract class Stimme {
	
	private int anzahl;
	private long wahlkreis;
	private boolean gueltig;
	
	public int getAnzahl() {
		return anzahl;
	}
	
	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}

	public long getWahlkreis() {
		return wahlkreis;
	}

	public void setWahlkreis(long wahlkreis) {
		this.wahlkreis = wahlkreis;
	}

	public boolean isGueltig() {
		return gueltig;
	}

	public void setGueltig(boolean gueltig) {
		this.gueltig = gueltig;
	}

}
