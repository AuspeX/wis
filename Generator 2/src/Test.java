import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Vector;

import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;

public class Test {
	
	//Formats
	//Zweitstimme: stimme_anzahl | stimme_wahlkreis | stimme_gueltig | zweitstimme_landesliste
	//Erststimme: stimme_anzahl | stimme_wahlkreis | stimme_gueltig | erststimme_direktkandidat

	private static final int ANZAHL = 0;
	private static final int WAHLKREIS = 1;
	private static final int GUELTIG = 2;
	private static final int LANDESLISTE = 3;
	private static final int DIREKTKANDIDAT = 3;
	
	private static HashMap<Long, Vector<Stimme>[]> map = new HashMap<Long, Vector<Stimme>[]>();
	
	private static int currentYear = -1;
	private static boolean alreadyExists;
//	private static int rowCount = 0;
//	private static int file_indx = 0;
	private static CsvWriter csvOutput;
	
    public static void main(String[] args) {
    	
//    	currentYear = Utils.YEAR_2009;
    	currentYear = Utils.YEAR_2005;
    	
    	read("data/_erst_"+currentYear+".csv","data/_zweit_"+currentYear+".csv");
    	String outputFile = "C:/Temp/result_"+currentYear+".csv";


        try {
        	alreadyExists = new File(outputFile).exists();
        	csvOutput = new CsvWriter(new FileWriter(outputFile, true), ';');
        	
        	int cnt = 0;
//        	Random rdm = new Random(System.currentTimeMillis());
//        	HashMap<Long, Vector<Stimme>[]> debug = map;
//        	Long tmp = new Long(rdm.nextInt(298)+1);
        	
//        	Vector[] vec = map.get(tmp);
//        	generatePollsheet(csvOutput,vec);
        	
        	int total = map.keySet().size();
        	
            for (Long val : map.keySet()) {
            	//TODO - just the first 5 districts
            	if (cnt == 6) {
            		break;
            	}
            	cnt++;
            	System.out.println(cnt+" of "+total);
            	Vector[] vec = map.get(val);
            	generatePollsheet(csvOutput,vec);
			}
            csvOutput.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    
    private static void generatePollsheet(CsvWriter writer,Vector[] vec) {
    	Vector es = vec[0];
    	Vector zw = vec[1];
    	while((vec[0].size() != 0) || (vec[1].size() != 0)) {
    		Pollsheet ps = new Pollsheet(currentYear);
    		Erststimme erst;
    		if (es.size() == 0) {
    			erst = null;
    		} else {
    			erst = (Erststimme) es.get(0); 
    		}
    		if (erst != null) {
    			ps.setBezirk(erst.getWahlkreis());
    			ps.setErststimme(erst.getDirektkandidat());
    			ps.setGueltig(erst.isGueltig());
    			
    			if (erst.getAnzahl() == 1) {
    				es.remove(0);
    			} else {
    				erst.setAnzahl(erst.getAnzahl()-1);
    			}
    		}
    		Zweitstimme zweit;
    		if (zw.size() == 0) {
    			zweit = null;
    		} else {
    			zweit = (Zweitstimme) zw.get(0);
    		}
    		if (zweit != null) {
    			ps.setBezirk(zweit.getWahlkreis());
    			ps.setZweitstimme(zweit.getLadesliste());
    			ps.setGueltig(zweit.isGueltig());
    			
    			if (zweit.getAnzahl() == 1) {
    				zw.remove(0);
    			} else {
    				zweit.setAnzahl(zweit.getAnzahl()-1);
    			}
    		}
    		write(writer, ps, alreadyExists);
    	}
    }
    
    private static void write(CsvWriter writer, Pollsheet sheet, boolean append) {
    	try {
    		writer.write(String.valueOf(currentYear));
            writer.write(String.valueOf(sheet.getErststimme()));
            writer.write(String.valueOf(sheet.getZweitstimme()));
            writer.write(String.valueOf(sheet.getBezirk()));
            writer.write("FALSE");
            writer.endRecord();
            
            writer.flush();
            
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    private static void read(String erst, String zweit) {
    	  try {

              CsvReader reader = new CsvReader(erst);
              
              while (reader.readRecord()) {
            	  Erststimme es = new Erststimme();
            	  es.setAnzahl(Integer.parseInt(reader.get(ANZAHL)));
            	  if (reader.get(DIREKTKANDIDAT).isEmpty()) {
            		  es.setDirektkandidat(-1);
            	  } else {
            		  es.setDirektkandidat(Long.parseLong(reader.get(DIREKTKANDIDAT)));
            	  }
            	  es.setGueltig(reader.get(GUELTIG).equals("t"));
           		  es.setWahlkreis(Long.parseLong(reader.get(WAHLKREIS)));
           		  
           		  if (es.getAnzahl() != 0) {
           			  insertIntoMap(es);
           		  }
              }
              reader.close();
              
              reader = new CsvReader(zweit);
              
              while (reader.readRecord()) {
            	  Zweitstimme zw = new Zweitstimme();
            	  zw.setAnzahl(Integer.parseInt(reader.get(ANZAHL)));
            	  if (reader.get(LANDESLISTE).isEmpty()) {
            		  zw.setLadesliste(-1);
            	  } else {
            		  zw.setLadesliste(Long.parseLong(reader.get(LANDESLISTE)));
            	  }
            	  zw.setGueltig(reader.get(GUELTIG).equals("t"));
                  zw.setWahlkreis(Long.parseLong(reader.get(WAHLKREIS)));
                  
                  if (zw.getAnzahl() != 0) {
                	  insertIntoMap(zw);
                  }
              }
              reader.close();

          } catch (FileNotFoundException e) {
              e.printStackTrace();
          } catch (IOException e) {
              e.printStackTrace();
          }
    }
    
    private static void insertIntoMap(Stimme st) {
    	if (st.getWahlkreis() < 1 || st.getWahlkreis() > 299) {
    		return;
    	}
    	Vector[] ary = new Vector[2];

    	if (map.get(st.getWahlkreis()) == null) {
    		Vector<Erststimme> vec_erst = new Vector<Erststimme>();
    		Vector<Zweitstimme> vec_zweit = new Vector<Zweitstimme>();
    		ary[0] = vec_erst;
    		ary[1] = vec_zweit;
    		
    		map.put(st.getWahlkreis(), ary);
    	} else {
    		ary = map.get(st.getWahlkreis());
    	}

    	if (st instanceof Erststimme) {
    		ary[0].add(st);
    	} else {
    		ary[1].add(st);
    	}
    }
    
}