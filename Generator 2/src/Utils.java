import java.util.Calendar;
import java.util.Random;


public class Utils {
	
	public static final int YEAR_2009 = 2009;
	public static final int YEAR_2005 = 2005;
	
    public static long randomTimestamp(int year) {
    	Calendar cal = Calendar.getInstance();
    	Random gen = new Random(cal.getTimeInMillis());
    	
    	if (year == YEAR_2005) {
    		cal.set(Calendar.YEAR, 2005);
    		cal.set(Calendar.DAY_OF_MONTH, 18);
    		cal.set(Calendar.MONTH, Calendar.SEPTEMBER);
    		cal.set(Calendar.HOUR, gen.nextInt(12));
    		cal.set(Calendar.MINUTE, gen.nextInt(60));
    		return cal.getTimeInMillis();
    	} else if (year == YEAR_2009) {
    		cal.set(Calendar.YEAR, 2009);
    		cal.set(Calendar.DAY_OF_MONTH, 27);
    		cal.set(Calendar.MONTH, Calendar.OCTOBER);
    		cal.set(Calendar.HOUR, gen.nextInt(12));
    		cal.set(Calendar.MINUTE, gen.nextInt(60));
    		return cal.getTimeInMillis();
    	} else {
    		return -1;
    	}
    }

}