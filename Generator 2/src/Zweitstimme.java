
public class Zweitstimme extends Stimme {

	private long ladesliste;

	public long getLadesliste() {
		return ladesliste;
	}

	public void setLadesliste(long ladesliste) {
		this.ladesliste = ladesliste;
	}
	
	@Override
	public String toString() {
		return "[stimme_anzahl="+getAnzahl()+",stimme_wahlkreis="+getWahlkreis()+",landesliste="+getLadesliste()+"]";
	}
}
