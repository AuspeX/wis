package de.tum.wahlautomat.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import de.tum.wahlautomat.shared.Wahlkreis;
import de.tum.wahlautomat.shared.WahlkreisInfo;
import java.util.*;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("vote")
public interface VoteService extends RemoteService 
{
	Boolean vote(int wahlkreis,String verificationCode, String stimme1, String stimm2) throws IllegalArgumentException;
	
	WahlkreisInfo getWahlkreisInfo(int wahlkreis) throws IllegalArgumentException;
	
	List<Wahlkreis> getWahlkreise() throws Exception;
}
