package de.tum.wahlautomat.client;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;

import de.tum.wahlautomat.shared.Wahlkreis;
import de.tum.wahlautomat.shared.WahlkreisInfo;

/**
 * The async counterpart of <code>VoteService</code>.
 */
public interface VoteServiceAsync 
{
	void vote(int wahlkreis, String verificationCode, String stimme1,
			String stimm2, AsyncCallback<Boolean> callback);

	void getWahlkreisInfo(int wahlkreis, AsyncCallback<WahlkreisInfo> callback);

	void getWahlkreise(AsyncCallback<List<Wahlkreis>> callback);
}
