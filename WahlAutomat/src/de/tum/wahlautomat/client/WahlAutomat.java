package de.tum.wahlautomat.client;

import java.util.List;

import de.tum.wahlautomat.shared.Direktkandidat;
import de.tum.wahlautomat.shared.FieldVerifier;
import de.tum.wahlautomat.shared.Landesliste;
import de.tum.wahlautomat.shared.Wahlkreis;
import de.tum.wahlautomat.shared.WahlkreisInfo;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class WahlAutomat implements EntryPoint {
	
	/**
	 * Create a remote service proxy to talk to the server-side Greeting service.
	 */
	private final VoteServiceAsync greetingService = GWT
			.create(VoteService.class);
	
	private List<Wahlkreis> moeglicheWahlkreise;

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() 
	{
		//Initialize Initial-GUI Objects
		final Label errorLabel = new Label();
		final ListBox wahlkreise = new ListBox();
		
		//Get Wahlkreise
		greetingService.getWahlkreise(new AsyncCallback<List<Wahlkreis>>() {

			@Override
			public void onFailure(Throwable caught) {
				//Unable to load
				errorLabel.setText("Die Wahlkreise konnten nicht ermittelt werden.");
			}

			@Override
			public void onSuccess(List<Wahlkreis> result) 
			{
				//Prepare wahlkreis list
				moeglicheWahlkreise = result;
				
				//Add dummy entry
				wahlkreise.addItem("Wahlkreis wählen", "");
				
				//Add real entries
				for(Wahlkreis w : result)
					wahlkreise.addItem(w.getId()+": "+w.getName(), ""+w.getId());

				//unselect
				wahlkreise.setSelectedIndex(-1);
				
				//Add List to panel (becomes visible)
				RootPanel.get("wahlkreisContainer").add(wahlkreise);
			}
		});
		
		//Initial Voting-GUI Objects
		final Label wahlinfoLabel = new Label();
		final TextBox codeBox = new TextBox();
		final Button resetDirektkandidat = new Button("Kein Direktkandidat");
		final Button resetPartei = new Button("Keine Partei");
		
		final Button voteButton = new Button("Abstimmen");
		
		final ListBox direktkandidaten = new ListBox();
		final ListBox parteien = new ListBox();
		
		//Set Wahlkreis handler
		wahlkreise.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				
				//Clear lists
				direktkandidaten.clear();
				parteien.clear();
				
				if(wahlkreise.getSelectedIndex() > 0)
				{
					//Get selected Wahlkreis
					Wahlkreis current = moeglicheWahlkreise.get(wahlkreise.getSelectedIndex()-1);
					wahlinfoLabel.setText(current.getId()+": "+current.getName()+" ("+current.getBundesland()+")");
					
					//Fill Lists
					greetingService.getWahlkreisInfo(wahlkreise.getSelectedIndex(),new AsyncCallback<WahlkreisInfo>() {
						public void onFailure(Throwable caught) {
							// Show the RPC error message to the user
							errorLabel.setText(caught.getMessage());
						}

						public void onSuccess(WahlkreisInfo result) 
						{
							//Received available direktkandidaten and landeslisten/parteien
		
							//Prepare GUI list direktkandidaten	
							for(Direktkandidat d : result.getDirekkandidaten())
								direktkandidaten.addItem(d.getNachname() + ", " + d.getVorname() + " (" + d.getParteiName() +")", ""+ d.getId());
							
							direktkandidaten.setVisibleItemCount(direktkandidaten.getItemCount());
							direktkandidaten.setSelectedIndex(-1);
							
							//Prepare GUI list parteien
							for(Landesliste l : result.getLandeslisten())
								parteien.addItem(l.getName(), ""+ l.getId());
							
							parteien.setVisibleItemCount(parteien.getItemCount());
							parteien.setSelectedIndex(-1);
							
							//Set votecontainer visible
							RootPanel.get("voteContainer").setVisible(true);
						}
					});
				}else{
					//Show error Message
					wahlinfoLabel.setText("");
					RootPanel.get("voteContainer").setVisible(false);
				}
			}
		});

		//Add the GUI-Objects into their container
		RootPanel.get("errorLabelContainer").add(errorLabel);
		RootPanel.get("wahlinfoContainer").add(wahlinfoLabel);
		
		//Setup voting Stuff
		RootPanel.get("codeContainer").add(codeBox);
		RootPanel.get("rdirektkandidatenContainer").add(resetDirektkandidat);
		RootPanel.get("direktkandidatenContainer").add(direktkandidaten);
		RootPanel.get("rparteienContainer").add(resetPartei);
		RootPanel.get("parteienContainer").add(parteien);
		RootPanel.get("voteButtonContainer").add(voteButton);
		
		RootPanel.get("voteContainer").setVisible(false);
		
		//Set reset direktkandidat handler
		resetDirektkandidat.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				//unset selection
				direktkandidaten.setSelectedIndex(-1);
			}
		});
		
		//Set reset partei handler
		resetPartei.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				//unset selection
				parteien.setSelectedIndex(-1);
			}
		});
		
		//set Vote handler
		voteButton.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				
					// First, we validate the input.
					errorLabel.setText("");
					
					String verificationCode = codeBox.getText();
					if (!FieldVerifier.isValidCode(verificationCode)) {
						errorLabel.setText("Der Code ist ungültig. Abstimmung nicht möglich!");
						return;
					}
					
					//Get selected direktkandidat
					String direktkandidat = null;
					
					if(direktkandidaten.getSelectedIndex() >= 0)
						direktkandidat = direktkandidaten.getValue(direktkandidaten.getSelectedIndex());
				
					//Get selected partei
					String partei = null;
					
					if(parteien.getSelectedIndex() >= 0)
						partei = parteien.getValue(parteien.getSelectedIndex());
					
					//Diasble button
					voteButton.setEnabled(false);
					
					//Get current wahlkreis
					Wahlkreis current = moeglicheWahlkreise.get(wahlkreise.getSelectedIndex()-1);
					
					//Do vote
					greetingService.vote(current.getId(),verificationCode,direktkandidat,partei,
							new AsyncCallback<Boolean>() {
								public void onFailure(Throwable caught) {
									// Vote could not be done
									errorLabel.setText(caught.getMessage());
									voteButton.setEnabled(true);
								}

								public void onSuccess(Boolean result) {
									
									//Check result
									if (result) {
										errorLabel.setText("Sie haben erfolgreich abgestimmt ("+direktkandidaten.getItemText(direktkandidaten.getSelectedIndex())+", "+parteien.getItemText(parteien.getSelectedIndex())+")");
									}else{
										errorLabel.setText("Uups. da ist etwas schiefgelaufen beim abstimmen");
									}
									
									//Disable GUI
									voteButton.setEnabled(false);
									resetDirektkandidat.setEnabled(false);
									resetPartei.setEnabled(false);
									
									wahlkreise.setEnabled(false);
									direktkandidaten.setEnabled(false);
									parteien.setEnabled(false);
									codeBox.setEnabled(false);
								}
							});
				}
		});
	}
}
