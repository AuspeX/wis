package de.tum.wahlautomat.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.postgresql.ds.PGPoolingDataSource;

import de.tum.wahlautomat.client.VoteService;
import de.tum.wahlautomat.shared.Direktkandidat;
import de.tum.wahlautomat.shared.FieldVerifier;
import de.tum.wahlautomat.shared.Landesliste;
import de.tum.wahlautomat.shared.Wahlkreis;
import de.tum.wahlautomat.shared.WahlkreisInfo;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class VoteServiceImpl extends RemoteServiceServlet implements
		VoteService 
		{

	PGPoolingDataSource dataSource;
	
	public VoteServiceImpl()
	{
		super();

			try 
			{
				//setup datasource
				dataSource = new PGPoolingDataSource();

				dataSource.setDataSourceName("postgres_wahlsystem");
				dataSource.setServerName("localhost");
				dataSource.setPortNumber(5432);
				dataSource.setDatabaseName("tmp");
				dataSource.setUser("postgres");
				dataSource.setPassword("postgres");
				dataSource.setMaxConnections(50);
			} catch (Exception e) {
				e.printStackTrace();
			}
	} 
	
	/**
	 * 
	 * Executes a vote
	 * 
	 * @param hash - hashcode to vote
	 * @param kandidat - direktkandidat to vote for
	 * @param landesliste - landesliste/partei to vote for
	 * 
	 * @return ?
	 */
	public Boolean vote(int wahlkreis, String hash, String kandidat, String landesliste) throws IllegalArgumentException {
		
		// Verify that the input is valid. 
		if (!FieldVerifier.isValidCode(hash)) {
			throw new IllegalArgumentException(
					"Der Code ist ungültig. Abstimmung nicht möglich!");
		}
		
		Connection con = null;
		try
		{
			PreparedStatement stmt = null;
			PreparedStatement stmt2 = null;
			PreparedStatement stmt3 = null;
			ResultSet rs = null;
			
			con = dataSource.getConnection();
			
			//Check if wahlkreis is valid
			try 
			{
				stmt = con.prepareStatement("SELECT wahlkreis_id FROM \"Wahlkreise\" WHERE wahlkreis_id = ?");
				stmt.setInt(1, wahlkreis);
				
				rs = stmt.executeQuery();
				
				if (!rs.next()) 
					throw new IllegalArgumentException(
							"Der Wahlkreis ist unbekannt");
			}catch(SQLException e){
				e.printStackTrace();
				return false;
			}finally{
				if (rs != null)
					try { rs.close();rs = null; } catch (SQLException e) {}
				if (stmt != null)
					try { stmt.close(); stmt = null; } catch (SQLException e) {}	
			}
			
			//Check if direktkandidat matches wahlkreis
			if(kandidat != null)
			{
				try 
				{
					stmt = con.prepareStatement("SELECT direktkandidat_wahlkreis FROM \"Direktkandidaten\" WHERE direktkandidat_id = ?");
					stmt.setInt(1, Integer.parseInt(kandidat));
					
					rs = stmt.executeQuery();
					
					if (rs.next())
					{
						int kandidatWahlkreis = rs.getInt(1);
						
						if(kandidatWahlkreis != wahlkreis)
							throw new IllegalArgumentException(
									"Der Kandidat passt nicht zum Wahlkreis.");
					} else
					{
						throw new IllegalArgumentException(
								"Unbekannter Kandidat");
						
					}
				}catch(SQLException e){
					e.printStackTrace();
					return false;
				}finally{
					if (rs != null)
						try { rs.close();rs = null; } catch (SQLException e) {}
					if (stmt != null)
						try { stmt.close(); stmt = null; } catch (SQLException e) {}	
				}
			}
			
			//Check if landesliste matches wahlkreis
			if(landesliste != null)
			{
				try 
				{
					stmt = con.prepareStatement("Select landesliste_id as id, wahlkreis_id as wahlkreis, landesliste_partei as partei from \"Wahlkreise\" INNER JOIN \"Landeslisten\" ON landesliste_bundesland = wahlkreis_bundesland "+
			"where landesliste_jahr = ? and wahlkreis_id = ? and landesliste_id = ?");
					
					stmt.setInt(1, 2009);
					stmt.setInt(2, wahlkreis);
					stmt.setInt(3, Integer.parseInt(landesliste));
					
					rs = stmt.executeQuery();
					
					if (!rs.next())
							throw new IllegalArgumentException(
									"Die Partei passt nicht zum Wahlkreis oder die Partei/Landesliste existiert nicht"); 
				}catch(SQLException e){
					e.printStackTrace();
					return false;
				}finally{
					if (rs != null)
						try { rs.close();rs = null; } catch (SQLException e) {}
					if (stmt != null)
						try { stmt.close(); stmt = null; } catch (SQLException e) {}	
				}
			}
			
			//Check if hash is valid
			try 
			{
				stmt = con.prepareStatement("SELECT wahlschluessel_benutzt FROM \"Wahlschluessel\" WHERE wahlschluessel_hash = ?");
				stmt.setString(1, hash.toLowerCase());
				
				rs = stmt.executeQuery();
				
				if (!rs.next()) 
					throw new IllegalArgumentException(
							"Der Code ist ungültig.");
				else
					if (rs.getBoolean(1)) 
						throw new IllegalArgumentException("Der Code ist ungültig. Doppeltes Abstimmen ist nicht erlaubt.");
				
			}catch(SQLException e){
				e.printStackTrace();
				return false;
			}finally{
				if (rs != null)
					try { rs.close();rs = null; } catch (SQLException e) {}
				if (stmt != null)
					try { stmt.close(); stmt = null; } catch (SQLException e) {}	
			}
			
			
			//Do vote
			try 
			{
				con.setAutoCommit(false);
				//Begin transaction
				
				//Invalidate wahlschlüssel
				stmt = con.prepareStatement("UPDATE \"Wahlschluessel\" SET wahlschluessel_benutzt = TRUE WHERE wahlschluessel_hash = ?");
				stmt.setString(1, hash);
				
				stmt.executeUpdate();
				
				//Check valid
				boolean valid = !(kandidat == null && landesliste == null);
				
				//Insert wahlzettel
				stmt = con.prepareStatement("INSERT INTO \"Wahlzettel\" VALUES (2009,?,?,?,?)");
				stmt2 = con.prepareStatement("UPDATE \"Erststimmen\" SET stimme_anzahl = stimme_anzahl+1 WHERE stimme_wahl=2009 AND stimme_gueltig=? AND stimme_wahlkreis=? AND erststimme_direktkandidat=?");
				stmt3 = con.prepareStatement("UPDATE \"Zweitstimmen\" SET stimme_anzahl = stimme_anzahl+1 WHERE stimme_wahl=2009 AND stimme_gueltig=? AND stimme_wahlkreis=? AND zweitstimme_landesliste=?");
				
				if(kandidat != null) {
					stmt.setInt(1, Integer.parseInt(kandidat));
					stmt2.setInt(3, Integer.parseInt(kandidat));
					stmt2.setBoolean(1, true);
				} else {
					stmt.setInt(1, -1);
					stmt2.setNull(3, Types.INTEGER);
					stmt2.setBoolean(1, false);
				}
				
				if(landesliste != null) {
					stmt.setInt(2, Integer.parseInt(landesliste));
					stmt3.setInt(3, Integer.parseInt(landesliste));
					stmt3.setBoolean(1, true);
				} else {
					stmt.setInt(2,-1);
					stmt3.setNull(3, Types.INTEGER);
					stmt3.setBoolean(1, false);
				}
						
				stmt.setInt(3, wahlkreis);
				stmt2.setInt(2, wahlkreis);
				stmt3.setInt(2, wahlkreis);
				stmt.setBoolean(4, valid);
				
				stmt.execute();
				stmt2.execute();
				stmt3.execute();
				
				//Commit changes
				con.commit();
				
			}catch(SQLException e){
				e.printStackTrace();
				return false;
			}finally{
				if (rs != null)
					try { rs.close();rs = null; } catch (SQLException e) {}
				if (stmt != null)
					try { stmt.close(); stmt = null; } catch (SQLException e) {}
				if (con != null)
					try { con.setAutoCommit(true); } catch (SQLException e) {}
			}
		}catch(SQLException e){
			e.printStackTrace();
			return false;
		}finally{
			if (con != null)
		        try { con.close(); } catch (SQLException e) {}
		}
		
		return true;
	}
	
	
	/**
	 * Returns the available list of parties(landeslisten) 
	 * that can be voted for in the given wahlkreis
	 * 
	 * @param wahlkreis
	 * @return list of landeslisten
	 */
	private List<Landesliste> getLandeslisten(int wahlkreis) {
		
		//Result
		List<Landesliste> landeslisten = new ArrayList<Landesliste>();
		
		
		//JDBC
		PreparedStatement stmt = null;
		Connection con = null;
		ResultSet rs = null;
		
		try 
		{
			con = dataSource.getConnection();
			
			//Prepare statement to load Landeslisten
			stmt = con.prepareStatement(
					"Select landesliste_id as id, wahlkreis_id as wahlkreis, landesliste_partei as partei from \"Wahlkreise\" INNER JOIN \"Landeslisten\" ON landesliste_bundesland = wahlkreis_bundesland "+
			"where landesliste_jahr = ? and wahlkreis_id = ?"
			);
			
			//set Parameter
			stmt.setInt(1, 2009);
			stmt.setInt(2, wahlkreis);

			//Executes the query an create objects
			rs = stmt.executeQuery();

		    while (rs.next()) 
		    	landeslisten.add(new Landesliste(rs.getInt(1), rs.getString(3),rs.getInt(2)));
		    
		} catch (SQLException e) {
			//Print Exception
			e.printStackTrace();
		}finally{
			//Close used resources
			if (rs != null)
				try { rs.close(); } catch (SQLException e) {}
			if (stmt != null)
				try { stmt.close(); } catch (SQLException e) {}
			if (con != null)
		        try { con.close(); } catch (SQLException e) {}
		}

		//Return result
		return landeslisten;
	}

	/**
	 * Returns the available List of politicians in the given wahlkreis
	 * 
	 * @param wahlkreis
	 * @return list of direktkandidaten
	 */
	private List<Direktkandidat> getDirektkandidaten(int wahlkreis) {
		
		//Result
		List<Direktkandidat> direktkandidaten = new ArrayList<Direktkandidat>();
		
		//JDBC
		PreparedStatement stmt = null;
		Connection con = null;
		ResultSet rs = null;
		
		try {
			con = dataSource.getConnection();
			//Prepare Statement to load Direktkandidaten
			stmt = con.prepareStatement(
					"Select direktkandidat_id as id, direktkandidat_wahlkreis as wahlkreis, politiker_vorname as vorname, politiker_nachname as nachname, mitglied_partei as partei from \"Direktkandidaten\" " + 
					"inner join \"Politiker\" ON direktkandidat_politiker = politiker_id "+
					"inner join \"Mitglieder\" ON mitglied_politiker = politiker_id and mitglied_jahr = direktkandidat_wahl "+
					"where direktkandidat_wahl = ? and direktkandidat_wahlkreis = ?"
			);
			
			//Set parameter
			stmt.setInt(1, 2009);
			stmt.setInt(2, wahlkreis);

			//Execute the query and create the result objects
			rs = stmt.executeQuery();

		    while (rs.next()) 
		    	direktkandidaten.add(new Direktkandidat(rs.getInt(1),rs.getInt(2),rs.getString(3),rs.getString(4),rs.getString(5)));
		    
		} catch (SQLException e) {
			//Print Exception
			e.printStackTrace();
		}finally{
			//Close used resources
			if (rs != null)
				try { rs.close(); } catch (SQLException e) {}
			if (stmt != null)
				try { stmt.close(); } catch (SQLException e) {}
			if (con != null)
		        try { con.close(); } catch (SQLException e) {}
		}

		//Return result
		return direktkandidaten;
	}

	/**
	 * Returns the available information for the given wahlkreis
	 * 
	 * @return wahlkreis information
	 */
	public WahlkreisInfo getWahlkreisInfo(int wahlkreis)
			throws IllegalArgumentException {
		
		//Check
		WahlkreisInfo info = new WahlkreisInfo();
		info.setWahlkreis(wahlkreis);
		
		//Set available Direktkandidaten
		List<Direktkandidat> direktkandidaten = this.getDirektkandidaten(wahlkreis);
		info.setDirekkandidaten(direktkandidaten);
		
		//Set available Parteien/Landeslisten
		List<Landesliste> landeslisten = this.getLandeslisten(wahlkreis);
		info.setLandeslisten(landeslisten);
		
		return info;
	}

	/**
	 * 
	 * Returns all available Wahlkreise
	 * 
	 * @return available wahlkreise
	 */
	public List<Wahlkreis> getWahlkreise() throws Exception 
	{
		//Result
		List<Wahlkreis> kreise = new ArrayList<Wahlkreis>();
		
		//JDBC
		Statement stmt = null;
		Connection con = null;
		ResultSet rs = null;
		
		try {
			//Get Data
			con = dataSource.getConnection();
			stmt = con.createStatement();

			rs = stmt.executeQuery("select wahlkreis_id as id, wahlkreis_name as name, bundesland_name as bundesland from \"Wahlkreise\" inner join \"Bundeslaender\" ON wahlkreis_bundesland = bundesland_id order by wahlkreis_id");

		    while (rs.next()) 
		    	kreise.add(new Wahlkreis(rs.getInt(1),rs.getString(2),rs.getString(3)));
		    
		} catch (SQLException e) {
			//Print exception
			e.printStackTrace();
		}finally{
			//Close used resources
			if (rs != null)
				try { rs.close(); } catch (SQLException e) {}
			if (stmt != null)
				try { stmt.close(); } catch (SQLException e) {}
			if (con != null)
		        try { con.close(); } catch (SQLException e) {}
		}

		//return result
		return kreise;
	}
}
