package de.tum.wahlautomat.shared;

import java.io.Serializable;

public class Direktkandidat implements Serializable{

	/**
	 * 
	 */
	
	private static final long serialVersionUID = -283726316169798376L;
	int id;
	int wahlkreis;
	String vorname;
	String nachname;
	String parteiName;
	
	public Direktkandidat(int id, int wahlkreis, String vorname,
			String nachname, String parteiName) {
		super();
		this.id = id;
		this.wahlkreis = wahlkreis;
		this.vorname = vorname;
		this.nachname = nachname;
		this.parteiName = parteiName;
	}
	
	

	public Direktkandidat() {
		super();
		// TODO Auto-generated constructor stub
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getWahlkreis() {
		return wahlkreis;
	}

	public void setWahlkreis(int wahlkreis) {
		this.wahlkreis = wahlkreis;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getNachname() {
		return nachname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	public String getParteiName() {
		return parteiName;
	}

	public void setParteiName(String parteiName) {
		this.parteiName = parteiName;
	}
}
