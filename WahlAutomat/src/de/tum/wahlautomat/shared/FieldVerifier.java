package de.tum.wahlautomat.shared;

public class FieldVerifier {

	public static boolean isValidCode(String code) {
		if (code == null) {
			return false;
		}
		return code != null && code.length() == 64;
	}
}
