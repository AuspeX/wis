package de.tum.wahlautomat.shared;

import java.io.Serializable;

public class Landesliste implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3598650563026260859L;
	
	int id;
	String name;
	int wahlkreis;
	
	public Landesliste(int id, String name, int wahlkreis) {
		super();
		this.id = id;
		this.name = name;
		this.wahlkreis = wahlkreis;
	}

	public Landesliste() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getWahlkreis() {
		return wahlkreis;
	}

	public void setWahlkreis(int wahlkreis) {
		this.wahlkreis = wahlkreis;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
}
