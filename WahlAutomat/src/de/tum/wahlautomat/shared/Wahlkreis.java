package de.tum.wahlautomat.shared;

import java.io.Serializable;

public class Wahlkreis implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	int id;
	String name;
	String bundesland;
	public Wahlkreis(int id, String name, String bundesland) {
		super();
		this.id = id;
		this.name = name;
		this.bundesland = bundesland;
	}
	public Wahlkreis() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBundesland() {
		return bundesland;
	}
	public void setBundesland(String bundesland) {
		this.bundesland = bundesland;
	}

}
