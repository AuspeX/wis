package de.tum.wahlautomat.shared;

import java.io.Serializable;
import java.util.List;

public class WahlkreisInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2360082679669127695L;

	int wahlkreis;
	
	List<Direktkandidat> direkkandidaten;
	List<Landesliste> landeslisten;
	
	
	public WahlkreisInfo(int wahlkreis, List<Direktkandidat> direkkandidaten,
			List<Landesliste> parteien) {
		super();
		this.wahlkreis = wahlkreis;
		this.direkkandidaten = direkkandidaten;
		this.landeslisten = parteien;
	}
	public WahlkreisInfo() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getWahlkreis() {
		return wahlkreis;
	}
	public void setWahlkreis(int wahlkreis) {
		this.wahlkreis = wahlkreis;
	}
	public List<Direktkandidat> getDirekkandidaten() {
		return direkkandidaten;
	}
	public void setDirekkandidaten(List<Direktkandidat> direkkandidaten) {
		this.direkkandidaten = direkkandidaten;
	}
	public List<Landesliste> getLandeslisten() {
		return landeslisten;
	}
	public void setLandeslisten(List<Landesliste> landeslisten) {
		this.landeslisten = landeslisten;
	}
	
	
}
