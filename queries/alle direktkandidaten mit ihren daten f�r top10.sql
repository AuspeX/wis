﻿CREATE VIEW "Result_Direktkandidaten_Ranked_2009_try" AS (SELECT pol.politiker_vorname, pol.politiker_nachname, e.stimme_anzahl, d.direktkandidat_wahlkreis, par.partei_name
FROM "Direktkandidaten" d, "Erststimmen" e, "Mitglieder" m, "Parteien" par, "Politiker" pol
WHERE e.erststimme_direktkandidat = d.direktkandidat_id
AND d.direktkandidat_politiker = pol.politiker_id
AND m.mitglied_partei = par.partei_id
AND m.mitglied_politiker = pol.politiker_id
AND m.mitglied_jahr = 2009 AND d.direktkandidat_wahl = 2009
ORDER BY direktkandidat_wahlkreis ASC, stimme_anzahl DESC)